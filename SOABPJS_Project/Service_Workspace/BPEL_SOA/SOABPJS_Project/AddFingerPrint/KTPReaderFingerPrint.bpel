<?xml version = "1.0" encoding = "UTF-8" ?>
<!--
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  Oracle JDeveloper BPEL Designer 
  
  Created: Thu Jul 26 17:00:55 ICT 2018
  Author:  gimel
  Type: BPEL 2.0 Process
  Purpose: Synchronous BPEL Process
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-->
<process name="KTPReaderFingerPrint"
               targetNamespace="http://xmlns.oracle.com/Application1/FingerPrint/KTPReaderFingerPrint"
               xmlns="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
               xmlns:client="http://xmlns.oracle.com/Application1/FingerPrint/KTPReaderFingerPrint"
               xmlns:ora="http://schemas.oracle.com/xpath/extension"
               xmlns:bpelx="http://schemas.oracle.com/bpel/extension"
         xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
         xmlns:ns1="http://www.example.org"
         xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/"
         xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
         xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
         xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
         xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath"
         xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath"
         xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions"
         xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk"
         xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
         xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap"
         xmlns:ns2="http://xmlns.oracle.com/pcbpel/adapter/db/Application1/FingerPrint/USP_ADD_PESERTAFINGER"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:ns3="http://xmlns.oracle.com/pcbpel/adapter/db/Application1/FingerPrint/USP_DATNKAPST_NIK"
         xmlns:ns4="http://xmlns.oracle.com/pcbpel/adapter/db/sp/USP_DATNKAPST_NIK"
         xmlns:ns5="http://xmlns.oracle.com/pcbpel/adapter/db/Application1/FingerPrint/USP_DATSEP_FINGER_INSERT"
         xmlns:ns6="http://xmlns.oracle.com/pcbpel/adapter/db/sp/USP_DATSEP_FINGER_INSERT"
         xmlns:ns7="http://xmlns.oracle.com/Application1/FingerPrint/PesertaTransformation"
         xmlns:ns8="http://xmlns.oracle.com/Application1/FingerPrint/GetPesertaByNik"
         xmlns:ns9="http://xmlns.oracle.com/Application1/FingerPrint/AddInsertSepFinger">

    <import namespace="http://xmlns.oracle.com/Application1/FingerPrint/KTPReaderFingerPrint" location="KTPReaderFingerPrint.wsdl" importType="http://schemas.xmlsoap.org/wsdl/"/>
    <!-- 
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        PARTNERLINKS                                                      
        List of services participating in this BPEL process               
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    --> 
  <partnerLinks>
    <!-- 
      The 'client' role represents the requester of this service. It is 
      used for callback. The location and correlation information associated
      with the client role are automatically set using WS-Addressing.
    -->
    <partnerLink name="ktpreaderfingerprint_client" partnerLinkType="client:KTPReaderFingerPrint" myRole="KTPReaderFingerPrintProvider"/>
    <partnerLink name="GetPesertaByNik.getpesertabynik_client"
                 partnerLinkType="ns8:GetPesertaByNik"
                 partnerRole="GetPesertaByNikProvider"/>
    <partnerLink name="AddInsertSepFinger.addinsertsepfinger_client"
                 partnerLinkType="ns9:AddInsertSepFinger"
                 partnerRole="AddInsertSepFingerProvider"/>
  </partnerLinks>

  <!-- 
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      VARIABLES                                                        
      List of messages and XML documents used within this BPEL process 
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  -->
  <variables>
    <!-- Reference to the message passed as input during initiation -->
    <variable name="inputVariable" messageType="client:KTPReaderFingerPrintRequestMessage"/>

    <!-- Reference to the message that will be returned to the requester-->
    <variable name="outputVariable" messageType="client:KTPReaderFingerPrintResponseMessage"/>
    <variable name="header"
              messageType="client:KTPReaderFingerPrintHeaderMessage"/>
    <variable name="GetPesertaByNik_process_InputVariable"
              messageType="ns8:GetPesertaByNikRequestMessage"/>
    <variable name="GetPesertaByNik_process_OutputVariable"
              messageType="ns8:GetPesertaByNikResponseMessage"/>
    <variable name="AddInsertSepFinger_process_InputVariable"
              messageType="ns9:AddInsertSepFingerRequestMessage"/>
    <variable name="AddInsertSepFinger_process_OutputVariable"
              messageType="ns9:AddInsertSepFingerResponseMessage"/>
  </variables>

  <!-- 
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     ORCHESTRATION LOGIC                                               
     Set of activities coordinating the flow of messages across the    
     services integrated within this business process                  
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  -->
  <sequence name="main">

    <!-- Receive input from requestor. (Note: This maps to operation defined in KTPReaderFingerPrint.wsdl) -->
    <receive name="receiveInput" partnerLink="ktpreaderfingerprint_client" portType="client:KTPReaderFingerPrint" operation="process" variable="inputVariable" createInstance="yes"
             bpelx:headerVariable="header"/>
    <if name="ValidasiKeywordInput">
      <documentation>
        <![CDATA[InvalidKeyword]]>
      </documentation>
      <condition>(normalize-space($inputVariable.payload/ns1:keyword)='') or (string-length($inputVariable.payload/ns1:keyword) != 16)</condition>
      <sequence name="Sequence4">
        <assign name="AssignFailedResponse">
          <copy>
            <from>1</from>
            <to>$outputVariable.payload/ns1:metaData/ns1:code</to>
          </copy>
          <copy>
            <from>'Keyword is invalid'</from>
            <to>$outputVariable.payload/ns1:metaData/ns1:message</to>
          </copy>
        </assign>
        <reply name="reply1" variable="outputVariable"
               partnerLink="ktpreaderfingerprint_client"
               portType="client:KTPReaderFingerPrint" operation="process"/>
        <exit name="Exit1"/>
      </sequence>
      <else>
        <documentation>
          <![CDATA[Valid]]>
        </documentation>
        <empty name="Continue"/>
      </else>
    </if>
    <assign name="AssignParameters">
      <copy>
        <from>$inputVariable.payload/ns1:keyword</from>
        <to>$GetPesertaByNik_process_InputVariable.payload/ns1:nik</to>
      </copy>
    </assign>
    <invoke name="GetPesertaByNik" bpelx:invokeAsDetail="no"
            partnerLink="GetPesertaByNik.getpesertabynik_client"
            portType="ns8:GetPesertaByNik" operation="process"
            inputVariable="GetPesertaByNik_process_InputVariable"
            outputVariable="GetPesertaByNik_process_OutputVariable"/>
    <if name="CekPeserta">
      <documentation>
        <![CDATA[Terdaftar]]>
      </documentation>
      <condition>$GetPesertaByNik_process_OutputVariable.payload/ns1:metaData/ns1:code = 200</condition>
      <sequence name="Sequence6">
        <assign name="AssignParameters">
          <copy ignoreMissingFromData="yes">
            <from>$header.payload/ns1:token</from>
            <to>$AddInsertSepFinger_process_InputVariable.payload/ns1:token</to>
          </copy>
          <copy ignoreMissingFromData="yes">
            <from>$GetPesertaByNik_process_OutputVariable.payload/ns1:response/ns1:noKartu</from>
            <to>$AddInsertSepFinger_process_InputVariable.payload/ns1:nokapst</to>
          </copy>
        </assign>
        <invoke name="AddInsertSepFinger" bpelx:invokeAsDetail="no"
                partnerLink="AddInsertSepFinger.addinsertsepfinger_client"
                portType="ns9:AddInsertSepFinger" operation="process"
                inputVariable="AddInsertSepFinger_process_InputVariable"
                outputVariable="AddInsertSepFinger_process_OutputVariable"/>
        <assign name="AssignResponse">
          <copy ignoreMissingFromData="yes">
            <from>$AddInsertSepFinger_process_OutputVariable.payload/ns1:code</from>
            <to>$outputVariable.payload/ns1:metaData/ns1:code</to>
          </copy>
          <copy ignoreMissingFromData="yes">
            <from>$AddInsertSepFinger_process_OutputVariable.payload/ns1:message</from>
            <to>$outputVariable.payload/ns1:metaData/ns1:message</to>
          </copy>
          <copy ignoreMissingFromData="yes">
            <from>$GetPesertaByNik_process_OutputVariable.payload/ns1:response</from>
            <to>$outputVariable.payload/ns1:response</to>
          </copy>
        </assign>
      </sequence>
      <else>
        <documentation>
          <![CDATA[TidakTerdaftar]]>
        </documentation>
        <assign name="AssignMetadata">
          <copy>
            <from>$GetPesertaByNik_process_OutputVariable.payload/ns1:metaData/ns1:code</from>
            <to>$outputVariable.payload/ns1:metaData/ns1:code</to>
          </copy>
          <copy>
            <from>$GetPesertaByNik_process_OutputVariable.payload/ns1:metaData/ns1:message</from>
            <to>$outputVariable.payload/ns1:metaData/ns1:message</to>
          </copy>
        </assign>
      </else>
    </if>
    <!-- Generate reply to synchronous request -->
    <reply name="replyOutput" partnerLink="ktpreaderfingerprint_client" portType="client:KTPReaderFingerPrint" operation="process" variable="outputVariable"/>
  </sequence>
</process>