declare namespace functx = "http://www.functx.com";
declare function functx:first-day-of-month
  ( $date as xs:anyAtomicType? )  as xs:date? {

   functx:date(year-from-date(xs:date($date)),
            month-from-date(xs:date($date)),
            1)
 } ;