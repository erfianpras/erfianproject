package com.service.kepesertaan;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class PendftrnBayiBaruLahir_Pojo {

	@XmlAttribute
	private String NIK;
	private String NAMA_LGKP;
	private String TMPT_LHR;
	private String TGL_LHR;
	private String PSNOKA;
	private String NOKA;
	private String KD_RS;
	private String JENIS_KLMIN;
	private String NO_HP;
	private String NO_KK;
	@XmlElement(name = "RootElement")
	public String getNik(){
		return NIK;
	}
	public void setNik(String NIK){
		this.NIK = NIK;
	}
	public String getNamaLgkp(){
		return NAMA_LGKP;
	}
	public void setNamaLgkp(String NAMA_LGKP){
		this.NAMA_LGKP = NAMA_LGKP;
	}
	public String getTmpTLhr(){
		return TMPT_LHR;
	}
	public void setTmptLhr(String TMPT_LHR){
		this.TMPT_LHR = TMPT_LHR;
	}
	public String getTglLhr(){
		return TGL_LHR;
	}
	public void setTglLhr(String TGL_LHR){
		this.TGL_LHR = TGL_LHR;
	}
	public String getPsNoka(){
		return PSNOKA;
	}
	public void setPsNoka(String PSNOKA){
		this.PSNOKA = PSNOKA;
	}
	public String getNoka(){
		return NOKA;
	}
	public void setNoka(String NOKA){
		this.NOKA = NOKA;
	}
	public String getKdRS(){
		return KD_RS;
	}
	public void setKdRs(String KD_RS){
		this.KD_RS = KD_RS;
	}
	public String getJnsKlmn(){
		return JENIS_KLMIN;
	}
	public void setJnsKlmn(String JENIS_KLMIN){
		this.JENIS_KLMIN = JENIS_KLMIN;
	}
	public String getNoHp(){
		return NO_HP;
	}
	public void setNoHp(String NO_HP){
		this.NO_HP = NO_HP;
	}
	public String getNoKK(){
		return NO_KK;
	}
	public void setNoKK(String NO_KK){
		this.NO_KK = NO_KK;
	}
}
