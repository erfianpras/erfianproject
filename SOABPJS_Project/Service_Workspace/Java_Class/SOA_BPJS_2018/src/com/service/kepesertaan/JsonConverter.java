package com.service.kepesertaan;

//import java.io.*;
//import java.util.List;
//
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
//import javax.xml.crypto.dsig.XMLObject;


import java.io.IOException;
import java.io.StringReader;
import java.lang.e
import javax.xml.bind.JAXB;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlObject.Factory;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class JsonConverter {

	public static void main(String [] args) {
		String jsonStr = "{\"NIK\": \"1472020510926609\", " +
				"\"NAMA_LGKP\": \"Anak Nyonya BB\"," +
				"\"TMPT_LHR\": \"JAKARTA\"," +
				"\"TGL_LHR\": \"21-06-2016\"," +
				"\"PSNOKA\": null," +
				"\"NOKA\": \"0001089107379\"," +
				"  \"KD_RS\": \"0106R001\"," +
				"\"JENIS_KLMIN\": \"1\"," +
				"\"NO_HP\": \"08119235344\"," +
				"\"NO_KK\": \"3572020510926677\"}";
		
		String xmlString ="<?xml version=\"1.0\"?>" +
				"<Result>" +
				"<TMPT_LHR>JAKARTA</TMPT_LHR>" +
				"<NAMA_LGKP>Anak Nyonya BB</NAMA_LGKP>" +
				"<NIK>1472020510926609</NIK>" +
				"</Result>" +
				"<Result>" +
				"<TMPT_LHR>JAKARTA</TMPT_LHR>" +
				"<NAMA_LGKP>Anak Nyonya BB</NAMA_LGKP>" +
				"<NIK>1472020510926609</NIK>" +
				"</Result>" +
				"<NO_KK>3572020510926677</NO_KK>" +
				"<KD_RS>0106R001</KD_RS>" +
				"<NO_HP>08119235344</NO_HP>" +
				"<JENIS_KLMIN>1</JENIS_KLMIN>" +
				"<NOKA>0001089107379</NOKA>" +
				"<PSNOKA>null</PSNOKA>" +
				"<TGL_LHR>21-06-2016</TGL_LHR>";
		
	
				getJsonToXml(jsonStr);
	}
	
	public static XmlObject getJsonToXml(String json) {
		
		String str = "{\"rootElemen\" : " + json +"}";
		XmlObject ret=null;
		String  xmlStr = null;
		try {			
			JSONObject jsonStr = new JSONObject(str);
			System.out.println(jsonStr);
			xmlStr = XML.toString(jsonStr);
			ret = Factory.parse(xmlStr);			
			System.out.println(ret);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (XmlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ret;
	}
	
	public static String getXmlToJson(XmlObject xmlob){
		int PRETTY_PRINT_INDENT_FACTOR = 4;
		String jsonFormat=null;
		try{
			XmlObject xmlDoc = XmlObject.Factory.parse(xmlob.newInputStream());
			String xmlStr = xmlDoc.toString();
			JSONObject xmlJSONObj = XML.toJSONObject(xmlStr);
			jsonFormat = xmlJSONObj.toString();
			System.out.println(xmlJSONObj);
		}catch(Exception e){
			e.printStackTrace();
		}		
		
		System.out.println(jsonFormat);
		return jsonFormat;
	}
}
