xquery version "1.0" encoding "UTF-8";
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/ESB/Resources/Config/ErrorCodeTable/";

declare function xf:ErrorCodeTable()
as element(*) {
<ErrorCodeConfig>
   <code>
            <error>
				<code>BEA-386100</code>
				<statusCode>9012</statusCode>
				<description>Authentication Error</description>
			</error>
			<error>
				<code>BEA-386101</code>
				<statusCode>9012</statusCode>
				<description>Authentication Error</description>
			</error>
			<error>
				<code>BEA-380000</code>
				<statusCode>9010</statusCode>
				<description>Generic Error</description>
			</error>
            <error>
				<code>BEA-380001</code>
				<statusCode>9010</statusCode>
				<description>Generic Error</description>
			</error>
			<error>
				<code>BEA-380002</code>
				<statusCode>9010</statusCode>
				<description>Generic Error</description>
			</error>
			<error>
				<code>BEA-382505</code>
				<statusCode>9010</statusCode>
				<description>Generic Error</description>
			</error>
			<error>
				<code>BEA-382556</code>
				<statusCode>9011</statusCode>
				<description>MFL Transformation Failed</description>
			</error>
			<error>
				<code>BEA-381501</code>
				<statusCode>9011</statusCode>
				<description>Timeout Error</description>
			</error>
			<error>
				<code>BEA-381304</code>
				<statusCode>9011</statusCode>
				<description>Timeout Error</description>
			</error>
			<error>
				<code>BEA-382510</code>
				<statusCode>9011</statusCode>
				<description>General transport error occured</description>
			</error>

			<error>
				<code>BEA-382500</code>
				<statusCode>9011</statusCode>
				<description>General transport error occured</description>
			</error>
			<error>
				<code>BEA-382515</code>
				<statusCode>9011</statusCode>
				<description>Java Callout Error</description>
			</error>
			<error>
				<code>BEA-382513</code>
				<statusCode>9011</statusCode>
				<description>XQuery Transformation Error</description>
			</error>
  </code>
</ErrorCodeConfig>
};


xf:ErrorCodeTable()