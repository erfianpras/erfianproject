package com.error.handler.converter;

import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectWriter;

import org.example.errorHandler.OutputDocument;
import org.example.errorHandler.ResponseMessage;
import com.error.handler.mapper.ErrorMapper;
import com.error.handler.pojo.ErrorResponse;

public class ErrorConverter {

	public static String getErrorMessageXMLtoJSON(XmlObject xmlobj){
		ObjectWriter objwrt = ErrorMapper.getErrorResponseWriter();
		ErrorResponse errorResp = new ErrorResponse();
		String json = null;
		try{
			XmlObject doc = XmlObject.Factory.parse(xmlobj.newXMLStreamReader());
			
			if (doc instanceof OutputDocument){
				OutputDocument ErrorMsgDoc = (OutputDocument) doc;
				ResponseMessage RespMsg = ErrorMsgDoc.getOutput();
				errorResp.setCode(RespMsg.getCode());
				errorResp.setDescription(RespMsg.getDescription());
				json = objwrt.writeValueAsString(errorResp);
				System.out.println("====== ERROR ======");
				System.out.println(json);
				System.out.println("====== END OF ERROR ======");
			}
		}
		catch(XmlException xe){
			xe.printStackTrace();
		}
		catch(JsonMappingException jsme){
			jsme.printStackTrace();
		}
		catch(IOException ie){
			ie.printStackTrace();
		}
		return json;
	}
	
}
