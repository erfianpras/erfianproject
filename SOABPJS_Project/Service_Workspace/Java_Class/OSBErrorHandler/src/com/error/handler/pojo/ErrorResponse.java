package com.error.handler.pojo;

public class ErrorResponse {

	private String code;
	private String description;
	
	public String getCode(){
		return code;
	}
	public void setCode(String Code){
		this.code=Code;
	}
	public String getDescription(){
		return description;
	}
	public void setDescription(String Description){
		this.description=Description;
	}
}
