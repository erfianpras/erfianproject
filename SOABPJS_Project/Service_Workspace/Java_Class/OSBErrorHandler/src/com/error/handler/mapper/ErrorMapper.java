package com.error.handler.mapper;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.error.handler.pojo.ErrorResponse;;
public class ErrorMapper {

	private static ObjectMapper mapper;
	private static ObjectWriter errorResponseWriter;
	private static ObjectReader errorResponseReader;
	
	static{
		mapper = new ObjectMapper();
		mapper.setSerializationConfig(mapper.getSerializationConfig().
				withSerializationInclusion(Inclusion.ALWAYS));
		errorResponseReader = mapper.reader(ErrorResponse.class);
		errorResponseWriter = mapper.writerWithType(ErrorResponse.class);
	}
	
	public static ObjectReader getErrorResponseReader(){
		return errorResponseReader;
	}
	public static ObjectWriter getErrorResponseWriter(){
		return errorResponseWriter;
	}
}
