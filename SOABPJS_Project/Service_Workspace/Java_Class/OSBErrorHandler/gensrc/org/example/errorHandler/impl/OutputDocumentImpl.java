/*
 * An XML document type.
 * Localname: output
 * Namespace: http://www.example.org/ErrorHandler
 * Java type: org.example.errorHandler.OutputDocument
 *
 * Automatically generated - do not modify.
 */
package org.example.errorHandler.impl;
/**
 * A document containing one output(@http://www.example.org/ErrorHandler) element.
 *
 * This is a complex type.
 */
public class OutputDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.example.errorHandler.OutputDocument
{
    private static final long serialVersionUID = 1L;
    
    public OutputDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OUTPUT$0 = 
        new javax.xml.namespace.QName("http://www.example.org/ErrorHandler", "output");
    
    
    /**
     * Gets the "output" element
     */
    public org.example.errorHandler.ResponseMessage getOutput()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.example.errorHandler.ResponseMessage target = null;
            target = (org.example.errorHandler.ResponseMessage)get_store().find_element_user(OUTPUT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "output" element
     */
    public void setOutput(org.example.errorHandler.ResponseMessage output)
    {
        generatedSetterHelperImpl(output, OUTPUT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "output" element
     */
    public org.example.errorHandler.ResponseMessage addNewOutput()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.example.errorHandler.ResponseMessage target = null;
            target = (org.example.errorHandler.ResponseMessage)get_store().add_element_user(OUTPUT$0);
            return target;
        }
    }
}
