//package app.domain.pcare;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.security.crypto.codec.Base64;

public class Authentic {
	private int userid;
	private String timestamp = "";
	private String hash = "";

	public Authentic(int userid, String timestamp, String hash) {
		super();
		this.userid = userid;
		this.timestamp = timestamp;
		this.hash = hash;
	}

	public Authentic() {
	}

	public void setTimeStampNow() {
		timestamp = (System.currentTimeMillis()/1000) + "";
	}

	public void setHashAuto(String key) {
		setTimeStampNow();
		byte[] hmacData = null;
		String data = (getUserid() + "&" + timestamp);

		try {
			SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");

			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(secretKey);
			hmacData = mac.doFinal(data.getBytes("UTF-8"));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		hash = new String(new Base64().encode(hmacData));
		System.out.println(hash);
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public String getHash() {
		return hash;
		
	}

	public void setHash(String hash) {
		this.hash = hash;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getUserid() {
		return userid;
	}

	public static void main(String[] args){
		String hashs = "S19yLjjbqlxhNL+lvVrbadXC0Ed4qzowBM6tmAMGw44=";
		Authentic auth = new Authentic(9614, "1537179139", "S19yLjjbqlxhNL+lvVrbadXC0Ed4qzowBM6tmAMGw44=");
		String Hash = auth.getHash();
		
	}
}
