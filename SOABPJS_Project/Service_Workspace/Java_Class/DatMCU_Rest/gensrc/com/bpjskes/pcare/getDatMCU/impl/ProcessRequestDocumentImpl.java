/*
 * An XML document type.
 * Localname: processRequest
 * Namespace: http://www.bpjskes.com/pcare/GetDatMCU
 * Java type: com.bpjskes.pcare.getDatMCU.ProcessRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.bpjskes.pcare.getDatMCU.impl;
/**
 * A document containing one processRequest(@http://www.bpjskes.com/pcare/GetDatMCU) element.
 *
 * This is a complex type.
 */
public class ProcessRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.ProcessRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public ProcessRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROCESSREQUEST$0 = 
        new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "processRequest");
    
    
    /**
     * Gets the "processRequest" element
     */
    public com.bpjskes.pcare.getDatMCU.Input getProcessRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Input target = null;
            target = (com.bpjskes.pcare.getDatMCU.Input)get_store().find_element_user(PROCESSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "processRequest" element
     */
    public void setProcessRequest(com.bpjskes.pcare.getDatMCU.Input processRequest)
    {
        generatedSetterHelperImpl(processRequest, PROCESSREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "processRequest" element
     */
    public com.bpjskes.pcare.getDatMCU.Input addNewProcessRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Input target = null;
            target = (com.bpjskes.pcare.getDatMCU.Input)get_store().add_element_user(PROCESSREQUEST$0);
            return target;
        }
    }
}
