/*
 * XML Type:  output
 * Namespace: http://www.bpjskes.com/pcare/GetDatMCU
 * Java type: com.bpjskes.pcare.getDatMCU.Output
 *
 * Automatically generated - do not modify.
 */
package com.bpjskes.pcare.getDatMCU;


/**
 * An XML output(@http://www.bpjskes.com/pcare/GetDatMCU).
 *
 * This is a complex type.
 */
public interface Output extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Output.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAA152DEEB4DE5391FE3157820507C54B").resolveHandle("outputc8e3type");
    
    /**
     * Gets the "response" element
     */
    com.bpjskes.pcare.getDatMCU.Output.Response getResponse();
    
    /**
     * Sets the "response" element
     */
    void setResponse(com.bpjskes.pcare.getDatMCU.Output.Response response);
    
    /**
     * Appends and returns a new empty "response" element
     */
    com.bpjskes.pcare.getDatMCU.Output.Response addNewResponse();
    
    /**
     * An XML response(@http://www.bpjskes.com/pcare/GetDatMCU).
     *
     * This is a complex type.
     */
    public interface Response extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Response.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAA152DEEB4DE5391FE3157820507C54B").resolveHandle("responsebfc0elemtype");
        
        /**
         * Gets the "count" element
         */
        java.lang.String getCount();
        
        /**
         * Gets (as xml) the "count" element
         */
        org.apache.xmlbeans.XmlString xgetCount();
        
        /**
         * Sets the "count" element
         */
        void setCount(java.lang.String count);
        
        /**
         * Sets (as xml) the "count" element
         */
        void xsetCount(org.apache.xmlbeans.XmlString count);
        
        /**
         * Gets the "list" element
         */
        com.bpjskes.pcare.getDatMCU.Output.Response.List getList();
        
        /**
         * Sets the "list" element
         */
        void setList(com.bpjskes.pcare.getDatMCU.Output.Response.List list);
        
        /**
         * Appends and returns a new empty "list" element
         */
        com.bpjskes.pcare.getDatMCU.Output.Response.List addNewList();
        
        /**
         * Gets the "metadata" element
         */
        com.bpjskes.pcare.getDatMCU.Output.Response.Metadata getMetadata();
        
        /**
         * Sets the "metadata" element
         */
        void setMetadata(com.bpjskes.pcare.getDatMCU.Output.Response.Metadata metadata);
        
        /**
         * Appends and returns a new empty "metadata" element
         */
        com.bpjskes.pcare.getDatMCU.Output.Response.Metadata addNewMetadata();
        
        /**
         * An XML list(@http://www.bpjskes.com/pcare/GetDatMCU).
         *
         * This is a complex type.
         */
        public interface List extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(List.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAA152DEEB4DE5391FE3157820507C54B").resolveHandle("list0646elemtype");
            
            /**
             * Gets the "kdMCU" element
             */
            java.lang.String getKdMCU();
            
            /**
             * Gets (as xml) the "kdMCU" element
             */
            org.apache.xmlbeans.XmlString xgetKdMCU();
            
            /**
             * Sets the "kdMCU" element
             */
            void setKdMCU(java.lang.String kdMCU);
            
            /**
             * Sets (as xml) the "kdMCU" element
             */
            void xsetKdMCU(org.apache.xmlbeans.XmlString kdMCU);
            
            /**
             * Gets the "noKunjungan" element
             */
            java.lang.String getNoKunjungan();
            
            /**
             * Gets (as xml) the "noKunjungan" element
             */
            org.apache.xmlbeans.XmlString xgetNoKunjungan();
            
            /**
             * Sets the "noKunjungan" element
             */
            void setNoKunjungan(java.lang.String noKunjungan);
            
            /**
             * Sets (as xml) the "noKunjungan" element
             */
            void xsetNoKunjungan(org.apache.xmlbeans.XmlString noKunjungan);
            
            /**
             * Gets the "kdProvider" element
             */
            java.lang.String getKdProvider();
            
            /**
             * Gets (as xml) the "kdProvider" element
             */
            org.apache.xmlbeans.XmlString xgetKdProvider();
            
            /**
             * Sets the "kdProvider" element
             */
            void setKdProvider(java.lang.String kdProvider);
            
            /**
             * Sets (as xml) the "kdProvider" element
             */
            void xsetKdProvider(org.apache.xmlbeans.XmlString kdProvider);
            
            /**
             * Gets the "tglPelayanan" element
             */
            java.lang.String getTglPelayanan();
            
            /**
             * Gets (as xml) the "tglPelayanan" element
             */
            org.apache.xmlbeans.XmlString xgetTglPelayanan();
            
            /**
             * Sets the "tglPelayanan" element
             */
            void setTglPelayanan(java.lang.String tglPelayanan);
            
            /**
             * Sets (as xml) the "tglPelayanan" element
             */
            void xsetTglPelayanan(org.apache.xmlbeans.XmlString tglPelayanan);
            
            /**
             * Gets the "tekananDarahSistole" element
             */
            java.lang.String getTekananDarahSistole();
            
            /**
             * Gets (as xml) the "tekananDarahSistole" element
             */
            org.apache.xmlbeans.XmlString xgetTekananDarahSistole();
            
            /**
             * Sets the "tekananDarahSistole" element
             */
            void setTekananDarahSistole(java.lang.String tekananDarahSistole);
            
            /**
             * Sets (as xml) the "tekananDarahSistole" element
             */
            void xsetTekananDarahSistole(org.apache.xmlbeans.XmlString tekananDarahSistole);
            
            /**
             * Gets the "radiologiFoto" element
             */
            java.lang.String getRadiologiFoto();
            
            /**
             * Gets (as xml) the "radiologiFoto" element
             */
            org.apache.xmlbeans.XmlString xgetRadiologiFoto();
            
            /**
             * Sets the "radiologiFoto" element
             */
            void setRadiologiFoto(java.lang.String radiologiFoto);
            
            /**
             * Sets (as xml) the "radiologiFoto" element
             */
            void xsetRadiologiFoto(org.apache.xmlbeans.XmlString radiologiFoto);
            
            /**
             * Gets the "darahRutinHemo" element
             */
            java.lang.String getDarahRutinHemo();
            
            /**
             * Gets (as xml) the "darahRutinHemo" element
             */
            org.apache.xmlbeans.XmlString xgetDarahRutinHemo();
            
            /**
             * Sets the "darahRutinHemo" element
             */
            void setDarahRutinHemo(java.lang.String darahRutinHemo);
            
            /**
             * Sets (as xml) the "darahRutinHemo" element
             */
            void xsetDarahRutinHemo(org.apache.xmlbeans.XmlString darahRutinHemo);
            
            /**
             * Gets the "darahRutinLeu" element
             */
            java.lang.String getDarahRutinLeu();
            
            /**
             * Gets (as xml) the "darahRutinLeu" element
             */
            org.apache.xmlbeans.XmlString xgetDarahRutinLeu();
            
            /**
             * Sets the "darahRutinLeu" element
             */
            void setDarahRutinLeu(java.lang.String darahRutinLeu);
            
            /**
             * Sets (as xml) the "darahRutinLeu" element
             */
            void xsetDarahRutinLeu(org.apache.xmlbeans.XmlString darahRutinLeu);
            
            /**
             * Gets the "darahRutinErit" element
             */
            java.lang.String getDarahRutinErit();
            
            /**
             * Gets (as xml) the "darahRutinErit" element
             */
            org.apache.xmlbeans.XmlString xgetDarahRutinErit();
            
            /**
             * Sets the "darahRutinErit" element
             */
            void setDarahRutinErit(java.lang.String darahRutinErit);
            
            /**
             * Sets (as xml) the "darahRutinErit" element
             */
            void xsetDarahRutinErit(org.apache.xmlbeans.XmlString darahRutinErit);
            
            /**
             * Gets the "darahRutinLaju" element
             */
            java.lang.String getDarahRutinLaju();
            
            /**
             * Gets (as xml) the "darahRutinLaju" element
             */
            org.apache.xmlbeans.XmlString xgetDarahRutinLaju();
            
            /**
             * Sets the "darahRutinLaju" element
             */
            void setDarahRutinLaju(java.lang.String darahRutinLaju);
            
            /**
             * Sets (as xml) the "darahRutinLaju" element
             */
            void xsetDarahRutinLaju(org.apache.xmlbeans.XmlString darahRutinLaju);
            
            /**
             * Gets the "darahRutinHema" element
             */
            java.lang.String getDarahRutinHema();
            
            /**
             * Gets (as xml) the "darahRutinHema" element
             */
            org.apache.xmlbeans.XmlString xgetDarahRutinHema();
            
            /**
             * Sets the "darahRutinHema" element
             */
            void setDarahRutinHema(java.lang.String darahRutinHema);
            
            /**
             * Sets (as xml) the "darahRutinHema" element
             */
            void xsetDarahRutinHema(org.apache.xmlbeans.XmlString darahRutinHema);
            
            /**
             * Gets the "darahRutinTrom" element
             */
            java.lang.String getDarahRutinTrom();
            
            /**
             * Gets (as xml) the "darahRutinTrom" element
             */
            org.apache.xmlbeans.XmlString xgetDarahRutinTrom();
            
            /**
             * Sets the "darahRutinTrom" element
             */
            void setDarahRutinTrom(java.lang.String darahRutinTrom);
            
            /**
             * Sets (as xml) the "darahRutinTrom" element
             */
            void xsetDarahRutinTrom(org.apache.xmlbeans.XmlString darahRutinTrom);
            
            /**
             * Gets the "lemakDarahHDL" element
             */
            java.lang.String getLemakDarahHDL();
            
            /**
             * Gets (as xml) the "lemakDarahHDL" element
             */
            org.apache.xmlbeans.XmlString xgetLemakDarahHDL();
            
            /**
             * Sets the "lemakDarahHDL" element
             */
            void setLemakDarahHDL(java.lang.String lemakDarahHDL);
            
            /**
             * Sets (as xml) the "lemakDarahHDL" element
             */
            void xsetLemakDarahHDL(org.apache.xmlbeans.XmlString lemakDarahHDL);
            
            /**
             * Gets the "lemakDarahLDL" element
             */
            java.lang.String getLemakDarahLDL();
            
            /**
             * Gets (as xml) the "lemakDarahLDL" element
             */
            org.apache.xmlbeans.XmlString xgetLemakDarahLDL();
            
            /**
             * Sets the "lemakDarahLDL" element
             */
            void setLemakDarahLDL(java.lang.String lemakDarahLDL);
            
            /**
             * Sets (as xml) the "lemakDarahLDL" element
             */
            void xsetLemakDarahLDL(org.apache.xmlbeans.XmlString lemakDarahLDL);
            
            /**
             * Gets the "lemakDarahChol" element
             */
            java.lang.String getLemakDarahChol();
            
            /**
             * Gets (as xml) the "lemakDarahChol" element
             */
            org.apache.xmlbeans.XmlString xgetLemakDarahChol();
            
            /**
             * Sets the "lemakDarahChol" element
             */
            void setLemakDarahChol(java.lang.String lemakDarahChol);
            
            /**
             * Sets (as xml) the "lemakDarahChol" element
             */
            void xsetLemakDarahChol(org.apache.xmlbeans.XmlString lemakDarahChol);
            
            /**
             * Gets the "lemakDarahTrigli" element
             */
            java.lang.String getLemakDarahTrigli();
            
            /**
             * Gets (as xml) the "lemakDarahTrigli" element
             */
            org.apache.xmlbeans.XmlString xgetLemakDarahTrigli();
            
            /**
             * Sets the "lemakDarahTrigli" element
             */
            void setLemakDarahTrigli(java.lang.String lemakDarahTrigli);
            
            /**
             * Sets (as xml) the "lemakDarahTrigli" element
             */
            void xsetLemakDarahTrigli(org.apache.xmlbeans.XmlString lemakDarahTrigli);
            
            /**
             * Gets the "gulaDarahSewaktu" element
             */
            java.lang.String getGulaDarahSewaktu();
            
            /**
             * Gets (as xml) the "gulaDarahSewaktu" element
             */
            org.apache.xmlbeans.XmlString xgetGulaDarahSewaktu();
            
            /**
             * Sets the "gulaDarahSewaktu" element
             */
            void setGulaDarahSewaktu(java.lang.String gulaDarahSewaktu);
            
            /**
             * Sets (as xml) the "gulaDarahSewaktu" element
             */
            void xsetGulaDarahSewaktu(org.apache.xmlbeans.XmlString gulaDarahSewaktu);
            
            /**
             * Gets the "gulaDarahPuasa" element
             */
            java.lang.String getGulaDarahPuasa();
            
            /**
             * Gets (as xml) the "gulaDarahPuasa" element
             */
            org.apache.xmlbeans.XmlString xgetGulaDarahPuasa();
            
            /**
             * Sets the "gulaDarahPuasa" element
             */
            void setGulaDarahPuasa(java.lang.String gulaDarahPuasa);
            
            /**
             * Sets (as xml) the "gulaDarahPuasa" element
             */
            void xsetGulaDarahPuasa(org.apache.xmlbeans.XmlString gulaDarahPuasa);
            
            /**
             * Gets the "gulaDarahPostPrandial" element
             */
            java.lang.String getGulaDarahPostPrandial();
            
            /**
             * Gets (as xml) the "gulaDarahPostPrandial" element
             */
            org.apache.xmlbeans.XmlString xgetGulaDarahPostPrandial();
            
            /**
             * Sets the "gulaDarahPostPrandial" element
             */
            void setGulaDarahPostPrandial(java.lang.String gulaDarahPostPrandial);
            
            /**
             * Sets (as xml) the "gulaDarahPostPrandial" element
             */
            void xsetGulaDarahPostPrandial(org.apache.xmlbeans.XmlString gulaDarahPostPrandial);
            
            /**
             * Gets the "fungsiHatiSGOT" element
             */
            java.lang.String getFungsiHatiSGOT();
            
            /**
             * Gets (as xml) the "fungsiHatiSGOT" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiHatiSGOT();
            
            /**
             * Sets the "fungsiHatiSGOT" element
             */
            void setFungsiHatiSGOT(java.lang.String fungsiHatiSGOT);
            
            /**
             * Sets (as xml) the "fungsiHatiSGOT" element
             */
            void xsetFungsiHatiSGOT(org.apache.xmlbeans.XmlString fungsiHatiSGOT);
            
            /**
             * Gets the "fungsiHatiSGPT" element
             */
            java.lang.String getFungsiHatiSGPT();
            
            /**
             * Gets (as xml) the "fungsiHatiSGPT" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiHatiSGPT();
            
            /**
             * Sets the "fungsiHatiSGPT" element
             */
            void setFungsiHatiSGPT(java.lang.String fungsiHatiSGPT);
            
            /**
             * Sets (as xml) the "fungsiHatiSGPT" element
             */
            void xsetFungsiHatiSGPT(org.apache.xmlbeans.XmlString fungsiHatiSGPT);
            
            /**
             * Gets the "fungsiHatiGamma" element
             */
            java.lang.String getFungsiHatiGamma();
            
            /**
             * Gets (as xml) the "fungsiHatiGamma" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiHatiGamma();
            
            /**
             * Sets the "fungsiHatiGamma" element
             */
            void setFungsiHatiGamma(java.lang.String fungsiHatiGamma);
            
            /**
             * Sets (as xml) the "fungsiHatiGamma" element
             */
            void xsetFungsiHatiGamma(org.apache.xmlbeans.XmlString fungsiHatiGamma);
            
            /**
             * Gets the "fungsiHatiProtKua" element
             */
            java.lang.String getFungsiHatiProtKua();
            
            /**
             * Gets (as xml) the "fungsiHatiProtKua" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiHatiProtKua();
            
            /**
             * Sets the "fungsiHatiProtKua" element
             */
            void setFungsiHatiProtKua(java.lang.String fungsiHatiProtKua);
            
            /**
             * Sets (as xml) the "fungsiHatiProtKua" element
             */
            void xsetFungsiHatiProtKua(org.apache.xmlbeans.XmlString fungsiHatiProtKua);
            
            /**
             * Gets the "fungsiHatiAlbumin" element
             */
            java.lang.String getFungsiHatiAlbumin();
            
            /**
             * Gets (as xml) the "fungsiHatiAlbumin" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiHatiAlbumin();
            
            /**
             * Sets the "fungsiHatiAlbumin" element
             */
            void setFungsiHatiAlbumin(java.lang.String fungsiHatiAlbumin);
            
            /**
             * Sets (as xml) the "fungsiHatiAlbumin" element
             */
            void xsetFungsiHatiAlbumin(org.apache.xmlbeans.XmlString fungsiHatiAlbumin);
            
            /**
             * Gets the "fungsiGinjalCrea" element
             */
            java.lang.String getFungsiGinjalCrea();
            
            /**
             * Gets (as xml) the "fungsiGinjalCrea" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiGinjalCrea();
            
            /**
             * Sets the "fungsiGinjalCrea" element
             */
            void setFungsiGinjalCrea(java.lang.String fungsiGinjalCrea);
            
            /**
             * Sets (as xml) the "fungsiGinjalCrea" element
             */
            void xsetFungsiGinjalCrea(org.apache.xmlbeans.XmlString fungsiGinjalCrea);
            
            /**
             * Gets the "fungsiGinjalUreum" element
             */
            java.lang.String getFungsiGinjalUreum();
            
            /**
             * Gets (as xml) the "fungsiGinjalUreum" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiGinjalUreum();
            
            /**
             * Sets the "fungsiGinjalUreum" element
             */
            void setFungsiGinjalUreum(java.lang.String fungsiGinjalUreum);
            
            /**
             * Sets (as xml) the "fungsiGinjalUreum" element
             */
            void xsetFungsiGinjalUreum(org.apache.xmlbeans.XmlString fungsiGinjalUreum);
            
            /**
             * Gets the "fungsiGinjalAsam" element
             */
            java.lang.String getFungsiGinjalAsam();
            
            /**
             * Gets (as xml) the "fungsiGinjalAsam" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiGinjalAsam();
            
            /**
             * Sets the "fungsiGinjalAsam" element
             */
            void setFungsiGinjalAsam(java.lang.String fungsiGinjalAsam);
            
            /**
             * Sets (as xml) the "fungsiGinjalAsam" element
             */
            void xsetFungsiGinjalAsam(org.apache.xmlbeans.XmlString fungsiGinjalAsam);
            
            /**
             * Gets the "fungsiJantungABI" element
             */
            java.lang.String getFungsiJantungABI();
            
            /**
             * Gets (as xml) the "fungsiJantungABI" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiJantungABI();
            
            /**
             * Sets the "fungsiJantungABI" element
             */
            void setFungsiJantungABI(java.lang.String fungsiJantungABI);
            
            /**
             * Sets (as xml) the "fungsiJantungABI" element
             */
            void xsetFungsiJantungABI(org.apache.xmlbeans.XmlString fungsiJantungABI);
            
            /**
             * Gets the "fungsiJantungEKG" element
             */
            java.lang.String getFungsiJantungEKG();
            
            /**
             * Gets (as xml) the "fungsiJantungEKG" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiJantungEKG();
            
            /**
             * Sets the "fungsiJantungEKG" element
             */
            void setFungsiJantungEKG(java.lang.String fungsiJantungEKG);
            
            /**
             * Sets (as xml) the "fungsiJantungEKG" element
             */
            void xsetFungsiJantungEKG(org.apache.xmlbeans.XmlString fungsiJantungEKG);
            
            /**
             * Gets the "fungsiJantungEcho" element
             */
            java.lang.String getFungsiJantungEcho();
            
            /**
             * Gets (as xml) the "fungsiJantungEcho" element
             */
            org.apache.xmlbeans.XmlString xgetFungsiJantungEcho();
            
            /**
             * Sets the "fungsiJantungEcho" element
             */
            void setFungsiJantungEcho(java.lang.String fungsiJantungEcho);
            
            /**
             * Sets (as xml) the "fungsiJantungEcho" element
             */
            void xsetFungsiJantungEcho(org.apache.xmlbeans.XmlString fungsiJantungEcho);
            
            /**
             * Gets the "urinRutin" element
             */
            java.lang.String getUrinRutin();
            
            /**
             * Gets (as xml) the "urinRutin" element
             */
            org.apache.xmlbeans.XmlString xgetUrinRutin();
            
            /**
             * Sets the "urinRutin" element
             */
            void setUrinRutin(java.lang.String urinRutin);
            
            /**
             * Sets (as xml) the "urinRutin" element
             */
            void xsetUrinRutin(org.apache.xmlbeans.XmlString urinRutin);
            
            /**
             * Gets the "fundusKopi" element
             */
            java.lang.String getFundusKopi();
            
            /**
             * Gets (as xml) the "fundusKopi" element
             */
            org.apache.xmlbeans.XmlString xgetFundusKopi();
            
            /**
             * Sets the "fundusKopi" element
             */
            void setFundusKopi(java.lang.String fundusKopi);
            
            /**
             * Sets (as xml) the "fundusKopi" element
             */
            void xsetFundusKopi(org.apache.xmlbeans.XmlString fundusKopi);
            
            /**
             * Gets the "pemeriksaanLain" element
             */
            java.lang.String getPemeriksaanLain();
            
            /**
             * Gets (as xml) the "pemeriksaanLain" element
             */
            org.apache.xmlbeans.XmlString xgetPemeriksaanLain();
            
            /**
             * Sets the "pemeriksaanLain" element
             */
            void setPemeriksaanLain(java.lang.String pemeriksaanLain);
            
            /**
             * Sets (as xml) the "pemeriksaanLain" element
             */
            void xsetPemeriksaanLain(org.apache.xmlbeans.XmlString pemeriksaanLain);
            
            /**
             * Gets the "keterangan" element
             */
            java.lang.String getKeterangan();
            
            /**
             * Gets (as xml) the "keterangan" element
             */
            org.apache.xmlbeans.XmlString xgetKeterangan();
            
            /**
             * Sets the "keterangan" element
             */
            void setKeterangan(java.lang.String keterangan);
            
            /**
             * Sets (as xml) the "keterangan" element
             */
            void xsetKeterangan(org.apache.xmlbeans.XmlString keterangan);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.bpjskes.pcare.getDatMCU.Output.Response.List newInstance() {
                  return (com.bpjskes.pcare.getDatMCU.Output.Response.List) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.bpjskes.pcare.getDatMCU.Output.Response.List newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.bpjskes.pcare.getDatMCU.Output.Response.List) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML metadata(@http://www.bpjskes.com/pcare/GetDatMCU).
         *
         * This is a complex type.
         */
        public interface Metadata extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Metadata.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAA152DEEB4DE5391FE3157820507C54B").resolveHandle("metadatab915elemtype");
            
            /**
             * Gets the "message" element
             */
            java.lang.String getMessage();
            
            /**
             * Gets (as xml) the "message" element
             */
            org.apache.xmlbeans.XmlString xgetMessage();
            
            /**
             * Sets the "message" element
             */
            void setMessage(java.lang.String message);
            
            /**
             * Sets (as xml) the "message" element
             */
            void xsetMessage(org.apache.xmlbeans.XmlString message);
            
            /**
             * Gets the "code" element
             */
            java.lang.String getCode();
            
            /**
             * Gets (as xml) the "code" element
             */
            org.apache.xmlbeans.XmlString xgetCode();
            
            /**
             * Sets the "code" element
             */
            void setCode(java.lang.String code);
            
            /**
             * Sets (as xml) the "code" element
             */
            void xsetCode(org.apache.xmlbeans.XmlString code);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.bpjskes.pcare.getDatMCU.Output.Response.Metadata newInstance() {
                  return (com.bpjskes.pcare.getDatMCU.Output.Response.Metadata) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.bpjskes.pcare.getDatMCU.Output.Response.Metadata newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.bpjskes.pcare.getDatMCU.Output.Response.Metadata) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.bpjskes.pcare.getDatMCU.Output.Response newInstance() {
              return (com.bpjskes.pcare.getDatMCU.Output.Response) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.bpjskes.pcare.getDatMCU.Output.Response newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.bpjskes.pcare.getDatMCU.Output.Response) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.bpjskes.pcare.getDatMCU.Output newInstance() {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.Output parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.bpjskes.pcare.getDatMCU.Output parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.bpjskes.pcare.getDatMCU.Output parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.bpjskes.pcare.getDatMCU.Output) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
