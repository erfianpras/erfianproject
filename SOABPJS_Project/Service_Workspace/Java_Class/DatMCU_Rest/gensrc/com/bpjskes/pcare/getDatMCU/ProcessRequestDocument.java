/*
 * An XML document type.
 * Localname: processRequest
 * Namespace: http://www.bpjskes.com/pcare/GetDatMCU
 * Java type: com.bpjskes.pcare.getDatMCU.ProcessRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.bpjskes.pcare.getDatMCU;


/**
 * A document containing one processRequest(@http://www.bpjskes.com/pcare/GetDatMCU) element.
 *
 * This is a complex type.
 */
public interface ProcessRequestDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ProcessRequestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAA152DEEB4DE5391FE3157820507C54B").resolveHandle("processrequest70d4doctype");
    
    /**
     * Gets the "processRequest" element
     */
    com.bpjskes.pcare.getDatMCU.Input getProcessRequest();
    
    /**
     * Sets the "processRequest" element
     */
    void setProcessRequest(com.bpjskes.pcare.getDatMCU.Input processRequest);
    
    /**
     * Appends and returns a new empty "processRequest" element
     */
    com.bpjskes.pcare.getDatMCU.Input addNewProcessRequest();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument newInstance() {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.bpjskes.pcare.getDatMCU.ProcessRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.bpjskes.pcare.getDatMCU.ProcessRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
