/*
 * An XML document type.
 * Localname: processResponse
 * Namespace: http://www.bpjskes.com/pcare/GetDatMCU
 * Java type: com.bpjskes.pcare.getDatMCU.ProcessResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.bpjskes.pcare.getDatMCU.impl;
/**
 * A document containing one processResponse(@http://www.bpjskes.com/pcare/GetDatMCU) element.
 *
 * This is a complex type.
 */
public class ProcessResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.ProcessResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public ProcessResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROCESSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "processResponse");
    
    
    /**
     * Gets the "processResponse" element
     */
    public com.bpjskes.pcare.getDatMCU.Output getProcessResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Output target = null;
            target = (com.bpjskes.pcare.getDatMCU.Output)get_store().find_element_user(PROCESSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "processResponse" element
     */
    public void setProcessResponse(com.bpjskes.pcare.getDatMCU.Output processResponse)
    {
        generatedSetterHelperImpl(processResponse, PROCESSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "processResponse" element
     */
    public com.bpjskes.pcare.getDatMCU.Output addNewProcessResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Output target = null;
            target = (com.bpjskes.pcare.getDatMCU.Output)get_store().add_element_user(PROCESSRESPONSE$0);
            return target;
        }
    }
}
