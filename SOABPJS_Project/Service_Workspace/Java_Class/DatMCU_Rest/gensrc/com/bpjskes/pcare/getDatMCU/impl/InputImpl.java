/*
 * XML Type:  input
 * Namespace: http://www.bpjskes.com/pcare/GetDatMCU
 * Java type: com.bpjskes.pcare.getDatMCU.Input
 *
 * Automatically generated - do not modify.
 */
package com.bpjskes.pcare.getDatMCU.impl;
/**
 * An XML input(@http://www.bpjskes.com/pcare/GetDatMCU).
 *
 * This is a complex type.
 */
public class InputImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.Input
{
    private static final long serialVersionUID = 1L;
    
    public InputImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REQUEST$0 = 
        new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "request");
    
    
    /**
     * Gets the "request" element
     */
    public com.bpjskes.pcare.getDatMCU.Input.Request getRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Input.Request target = null;
            target = (com.bpjskes.pcare.getDatMCU.Input.Request)get_store().find_element_user(REQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "request" element
     */
    public void setRequest(com.bpjskes.pcare.getDatMCU.Input.Request request)
    {
        generatedSetterHelperImpl(request, REQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "request" element
     */
    public com.bpjskes.pcare.getDatMCU.Input.Request addNewRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Input.Request target = null;
            target = (com.bpjskes.pcare.getDatMCU.Input.Request)get_store().add_element_user(REQUEST$0);
            return target;
        }
    }
    /**
     * An XML request(@http://www.bpjskes.com/pcare/GetDatMCU).
     *
     * This is a complex type.
     */
    public static class RequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.Input.Request
    {
        private static final long serialVersionUID = 1L;
        
        public RequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName NOKUNJUNGAN$0 = 
            new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "noKunjungan");
        
        
        /**
         * Gets the "noKunjungan" element
         */
        public java.lang.String getNoKunjungan()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOKUNJUNGAN$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "noKunjungan" element
         */
        public org.apache.xmlbeans.XmlString xgetNoKunjungan()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOKUNJUNGAN$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "noKunjungan" element
         */
        public void setNoKunjungan(java.lang.String noKunjungan)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOKUNJUNGAN$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOKUNJUNGAN$0);
                }
                target.setStringValue(noKunjungan);
            }
        }
        
        /**
         * Sets (as xml) the "noKunjungan" element
         */
        public void xsetNoKunjungan(org.apache.xmlbeans.XmlString noKunjungan)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOKUNJUNGAN$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NOKUNJUNGAN$0);
                }
                target.set(noKunjungan);
            }
        }
    }
}
