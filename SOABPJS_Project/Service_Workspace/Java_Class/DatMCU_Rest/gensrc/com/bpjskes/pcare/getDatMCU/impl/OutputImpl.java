/*
 * XML Type:  output
 * Namespace: http://www.bpjskes.com/pcare/GetDatMCU
 * Java type: com.bpjskes.pcare.getDatMCU.Output
 *
 * Automatically generated - do not modify.
 */
package com.bpjskes.pcare.getDatMCU.impl;
/**
 * An XML output(@http://www.bpjskes.com/pcare/GetDatMCU).
 *
 * This is a complex type.
 */
public class OutputImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.Output
{
    private static final long serialVersionUID = 1L;
    
    public OutputImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESPONSE$0 = 
        new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "response");
    
    
    /**
     * Gets the "response" element
     */
    public com.bpjskes.pcare.getDatMCU.Output.Response getResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Output.Response target = null;
            target = (com.bpjskes.pcare.getDatMCU.Output.Response)get_store().find_element_user(RESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "response" element
     */
    public void setResponse(com.bpjskes.pcare.getDatMCU.Output.Response response)
    {
        generatedSetterHelperImpl(response, RESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "response" element
     */
    public com.bpjskes.pcare.getDatMCU.Output.Response addNewResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.bpjskes.pcare.getDatMCU.Output.Response target = null;
            target = (com.bpjskes.pcare.getDatMCU.Output.Response)get_store().add_element_user(RESPONSE$0);
            return target;
        }
    }
    /**
     * An XML response(@http://www.bpjskes.com/pcare/GetDatMCU).
     *
     * This is a complex type.
     */
    public static class ResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.Output.Response
    {
        private static final long serialVersionUID = 1L;
        
        public ResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName COUNT$0 = 
            new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "count");
        private static final javax.xml.namespace.QName LIST$2 = 
            new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "list");
        private static final javax.xml.namespace.QName METADATA$4 = 
            new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "metadata");
        
        
        /**
         * Gets the "count" element
         */
        public java.lang.String getCount()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNT$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "count" element
         */
        public org.apache.xmlbeans.XmlString xgetCount()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(COUNT$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "count" element
         */
        public void setCount(java.lang.String count)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNT$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COUNT$0);
                }
                target.setStringValue(count);
            }
        }
        
        /**
         * Sets (as xml) the "count" element
         */
        public void xsetCount(org.apache.xmlbeans.XmlString count)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(COUNT$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(COUNT$0);
                }
                target.set(count);
            }
        }
        
        /**
         * Gets the "list" element
         */
        public com.bpjskes.pcare.getDatMCU.Output.Response.List getList()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.bpjskes.pcare.getDatMCU.Output.Response.List target = null;
                target = (com.bpjskes.pcare.getDatMCU.Output.Response.List)get_store().find_element_user(LIST$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "list" element
         */
        public void setList(com.bpjskes.pcare.getDatMCU.Output.Response.List list)
        {
            generatedSetterHelperImpl(list, LIST$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "list" element
         */
        public com.bpjskes.pcare.getDatMCU.Output.Response.List addNewList()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.bpjskes.pcare.getDatMCU.Output.Response.List target = null;
                target = (com.bpjskes.pcare.getDatMCU.Output.Response.List)get_store().add_element_user(LIST$2);
                return target;
            }
        }
        
        /**
         * Gets the "metadata" element
         */
        public com.bpjskes.pcare.getDatMCU.Output.Response.Metadata getMetadata()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.bpjskes.pcare.getDatMCU.Output.Response.Metadata target = null;
                target = (com.bpjskes.pcare.getDatMCU.Output.Response.Metadata)get_store().find_element_user(METADATA$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "metadata" element
         */
        public void setMetadata(com.bpjskes.pcare.getDatMCU.Output.Response.Metadata metadata)
        {
            generatedSetterHelperImpl(metadata, METADATA$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "metadata" element
         */
        public com.bpjskes.pcare.getDatMCU.Output.Response.Metadata addNewMetadata()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.bpjskes.pcare.getDatMCU.Output.Response.Metadata target = null;
                target = (com.bpjskes.pcare.getDatMCU.Output.Response.Metadata)get_store().add_element_user(METADATA$4);
                return target;
            }
        }
        /**
         * An XML list(@http://www.bpjskes.com/pcare/GetDatMCU).
         *
         * This is a complex type.
         */
        public static class ListImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.Output.Response.List
        {
            private static final long serialVersionUID = 1L;
            
            public ListImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName KDMCU$0 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "kdMCU");
            private static final javax.xml.namespace.QName NOKUNJUNGAN$2 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "noKunjungan");
            private static final javax.xml.namespace.QName KDPROVIDER$4 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "kdProvider");
            private static final javax.xml.namespace.QName TGLPELAYANAN$6 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "tglPelayanan");
            private static final javax.xml.namespace.QName TEKANANDARAHSISTOLE$8 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "tekananDarahSistole");
            private static final javax.xml.namespace.QName RADIOLOGIFOTO$10 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "radiologiFoto");
            private static final javax.xml.namespace.QName DARAHRUTINHEMO$12 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "darahRutinHemo");
            private static final javax.xml.namespace.QName DARAHRUTINLEU$14 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "darahRutinLeu");
            private static final javax.xml.namespace.QName DARAHRUTINERIT$16 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "darahRutinErit");
            private static final javax.xml.namespace.QName DARAHRUTINLAJU$18 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "darahRutinLaju");
            private static final javax.xml.namespace.QName DARAHRUTINHEMA$20 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "darahRutinHema");
            private static final javax.xml.namespace.QName DARAHRUTINTROM$22 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "darahRutinTrom");
            private static final javax.xml.namespace.QName LEMAKDARAHHDL$24 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "lemakDarahHDL");
            private static final javax.xml.namespace.QName LEMAKDARAHLDL$26 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "lemakDarahLDL");
            private static final javax.xml.namespace.QName LEMAKDARAHCHOL$28 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "lemakDarahChol");
            private static final javax.xml.namespace.QName LEMAKDARAHTRIGLI$30 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "lemakDarahTrigli");
            private static final javax.xml.namespace.QName GULADARAHSEWAKTU$32 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "gulaDarahSewaktu");
            private static final javax.xml.namespace.QName GULADARAHPUASA$34 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "gulaDarahPuasa");
            private static final javax.xml.namespace.QName GULADARAHPOSTPRANDIAL$36 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "gulaDarahPostPrandial");
            private static final javax.xml.namespace.QName FUNGSIHATISGOT$38 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiHatiSGOT");
            private static final javax.xml.namespace.QName FUNGSIHATISGPT$40 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiHatiSGPT");
            private static final javax.xml.namespace.QName FUNGSIHATIGAMMA$42 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiHatiGamma");
            private static final javax.xml.namespace.QName FUNGSIHATIPROTKUA$44 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiHatiProtKua");
            private static final javax.xml.namespace.QName FUNGSIHATIALBUMIN$46 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiHatiAlbumin");
            private static final javax.xml.namespace.QName FUNGSIGINJALCREA$48 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiGinjalCrea");
            private static final javax.xml.namespace.QName FUNGSIGINJALUREUM$50 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiGinjalUreum");
            private static final javax.xml.namespace.QName FUNGSIGINJALASAM$52 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiGinjalAsam");
            private static final javax.xml.namespace.QName FUNGSIJANTUNGABI$54 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiJantungABI");
            private static final javax.xml.namespace.QName FUNGSIJANTUNGEKG$56 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiJantungEKG");
            private static final javax.xml.namespace.QName FUNGSIJANTUNGECHO$58 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fungsiJantungEcho");
            private static final javax.xml.namespace.QName URINRUTIN$60 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "urinRutin");
            private static final javax.xml.namespace.QName FUNDUSKOPI$62 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "fundusKopi");
            private static final javax.xml.namespace.QName PEMERIKSAANLAIN$64 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "pemeriksaanLain");
            private static final javax.xml.namespace.QName KETERANGAN$66 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "keterangan");
            
            
            /**
             * Gets the "kdMCU" element
             */
            public java.lang.String getKdMCU()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KDMCU$0, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "kdMCU" element
             */
            public org.apache.xmlbeans.XmlString xgetKdMCU()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KDMCU$0, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "kdMCU" element
             */
            public void setKdMCU(java.lang.String kdMCU)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KDMCU$0, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KDMCU$0);
                    }
                    target.setStringValue(kdMCU);
                }
            }
            
            /**
             * Sets (as xml) the "kdMCU" element
             */
            public void xsetKdMCU(org.apache.xmlbeans.XmlString kdMCU)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KDMCU$0, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KDMCU$0);
                    }
                    target.set(kdMCU);
                }
            }
            
            /**
             * Gets the "noKunjungan" element
             */
            public java.lang.String getNoKunjungan()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOKUNJUNGAN$2, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "noKunjungan" element
             */
            public org.apache.xmlbeans.XmlString xgetNoKunjungan()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOKUNJUNGAN$2, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "noKunjungan" element
             */
            public void setNoKunjungan(java.lang.String noKunjungan)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOKUNJUNGAN$2, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOKUNJUNGAN$2);
                    }
                    target.setStringValue(noKunjungan);
                }
            }
            
            /**
             * Sets (as xml) the "noKunjungan" element
             */
            public void xsetNoKunjungan(org.apache.xmlbeans.XmlString noKunjungan)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOKUNJUNGAN$2, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NOKUNJUNGAN$2);
                    }
                    target.set(noKunjungan);
                }
            }
            
            /**
             * Gets the "kdProvider" element
             */
            public java.lang.String getKdProvider()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KDPROVIDER$4, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "kdProvider" element
             */
            public org.apache.xmlbeans.XmlString xgetKdProvider()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KDPROVIDER$4, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "kdProvider" element
             */
            public void setKdProvider(java.lang.String kdProvider)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KDPROVIDER$4, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KDPROVIDER$4);
                    }
                    target.setStringValue(kdProvider);
                }
            }
            
            /**
             * Sets (as xml) the "kdProvider" element
             */
            public void xsetKdProvider(org.apache.xmlbeans.XmlString kdProvider)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KDPROVIDER$4, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KDPROVIDER$4);
                    }
                    target.set(kdProvider);
                }
            }
            
            /**
             * Gets the "tglPelayanan" element
             */
            public java.lang.String getTglPelayanan()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TGLPELAYANAN$6, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "tglPelayanan" element
             */
            public org.apache.xmlbeans.XmlString xgetTglPelayanan()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TGLPELAYANAN$6, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "tglPelayanan" element
             */
            public void setTglPelayanan(java.lang.String tglPelayanan)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TGLPELAYANAN$6, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TGLPELAYANAN$6);
                    }
                    target.setStringValue(tglPelayanan);
                }
            }
            
            /**
             * Sets (as xml) the "tglPelayanan" element
             */
            public void xsetTglPelayanan(org.apache.xmlbeans.XmlString tglPelayanan)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TGLPELAYANAN$6, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TGLPELAYANAN$6);
                    }
                    target.set(tglPelayanan);
                }
            }
            
            /**
             * Gets the "tekananDarahSistole" element
             */
            public java.lang.String getTekananDarahSistole()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TEKANANDARAHSISTOLE$8, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "tekananDarahSistole" element
             */
            public org.apache.xmlbeans.XmlString xgetTekananDarahSistole()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TEKANANDARAHSISTOLE$8, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "tekananDarahSistole" element
             */
            public void setTekananDarahSistole(java.lang.String tekananDarahSistole)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TEKANANDARAHSISTOLE$8, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TEKANANDARAHSISTOLE$8);
                    }
                    target.setStringValue(tekananDarahSistole);
                }
            }
            
            /**
             * Sets (as xml) the "tekananDarahSistole" element
             */
            public void xsetTekananDarahSistole(org.apache.xmlbeans.XmlString tekananDarahSistole)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TEKANANDARAHSISTOLE$8, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TEKANANDARAHSISTOLE$8);
                    }
                    target.set(tekananDarahSistole);
                }
            }
            
            /**
             * Gets the "radiologiFoto" element
             */
            public java.lang.String getRadiologiFoto()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RADIOLOGIFOTO$10, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "radiologiFoto" element
             */
            public org.apache.xmlbeans.XmlString xgetRadiologiFoto()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RADIOLOGIFOTO$10, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "radiologiFoto" element
             */
            public void setRadiologiFoto(java.lang.String radiologiFoto)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RADIOLOGIFOTO$10, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RADIOLOGIFOTO$10);
                    }
                    target.setStringValue(radiologiFoto);
                }
            }
            
            /**
             * Sets (as xml) the "radiologiFoto" element
             */
            public void xsetRadiologiFoto(org.apache.xmlbeans.XmlString radiologiFoto)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RADIOLOGIFOTO$10, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RADIOLOGIFOTO$10);
                    }
                    target.set(radiologiFoto);
                }
            }
            
            /**
             * Gets the "darahRutinHemo" element
             */
            public java.lang.String getDarahRutinHemo()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINHEMO$12, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "darahRutinHemo" element
             */
            public org.apache.xmlbeans.XmlString xgetDarahRutinHemo()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINHEMO$12, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "darahRutinHemo" element
             */
            public void setDarahRutinHemo(java.lang.String darahRutinHemo)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINHEMO$12, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DARAHRUTINHEMO$12);
                    }
                    target.setStringValue(darahRutinHemo);
                }
            }
            
            /**
             * Sets (as xml) the "darahRutinHemo" element
             */
            public void xsetDarahRutinHemo(org.apache.xmlbeans.XmlString darahRutinHemo)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINHEMO$12, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DARAHRUTINHEMO$12);
                    }
                    target.set(darahRutinHemo);
                }
            }
            
            /**
             * Gets the "darahRutinLeu" element
             */
            public java.lang.String getDarahRutinLeu()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINLEU$14, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "darahRutinLeu" element
             */
            public org.apache.xmlbeans.XmlString xgetDarahRutinLeu()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINLEU$14, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "darahRutinLeu" element
             */
            public void setDarahRutinLeu(java.lang.String darahRutinLeu)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINLEU$14, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DARAHRUTINLEU$14);
                    }
                    target.setStringValue(darahRutinLeu);
                }
            }
            
            /**
             * Sets (as xml) the "darahRutinLeu" element
             */
            public void xsetDarahRutinLeu(org.apache.xmlbeans.XmlString darahRutinLeu)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINLEU$14, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DARAHRUTINLEU$14);
                    }
                    target.set(darahRutinLeu);
                }
            }
            
            /**
             * Gets the "darahRutinErit" element
             */
            public java.lang.String getDarahRutinErit()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINERIT$16, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "darahRutinErit" element
             */
            public org.apache.xmlbeans.XmlString xgetDarahRutinErit()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINERIT$16, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "darahRutinErit" element
             */
            public void setDarahRutinErit(java.lang.String darahRutinErit)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINERIT$16, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DARAHRUTINERIT$16);
                    }
                    target.setStringValue(darahRutinErit);
                }
            }
            
            /**
             * Sets (as xml) the "darahRutinErit" element
             */
            public void xsetDarahRutinErit(org.apache.xmlbeans.XmlString darahRutinErit)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINERIT$16, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DARAHRUTINERIT$16);
                    }
                    target.set(darahRutinErit);
                }
            }
            
            /**
             * Gets the "darahRutinLaju" element
             */
            public java.lang.String getDarahRutinLaju()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINLAJU$18, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "darahRutinLaju" element
             */
            public org.apache.xmlbeans.XmlString xgetDarahRutinLaju()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINLAJU$18, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "darahRutinLaju" element
             */
            public void setDarahRutinLaju(java.lang.String darahRutinLaju)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINLAJU$18, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DARAHRUTINLAJU$18);
                    }
                    target.setStringValue(darahRutinLaju);
                }
            }
            
            /**
             * Sets (as xml) the "darahRutinLaju" element
             */
            public void xsetDarahRutinLaju(org.apache.xmlbeans.XmlString darahRutinLaju)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINLAJU$18, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DARAHRUTINLAJU$18);
                    }
                    target.set(darahRutinLaju);
                }
            }
            
            /**
             * Gets the "darahRutinHema" element
             */
            public java.lang.String getDarahRutinHema()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINHEMA$20, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "darahRutinHema" element
             */
            public org.apache.xmlbeans.XmlString xgetDarahRutinHema()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINHEMA$20, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "darahRutinHema" element
             */
            public void setDarahRutinHema(java.lang.String darahRutinHema)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINHEMA$20, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DARAHRUTINHEMA$20);
                    }
                    target.setStringValue(darahRutinHema);
                }
            }
            
            /**
             * Sets (as xml) the "darahRutinHema" element
             */
            public void xsetDarahRutinHema(org.apache.xmlbeans.XmlString darahRutinHema)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINHEMA$20, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DARAHRUTINHEMA$20);
                    }
                    target.set(darahRutinHema);
                }
            }
            
            /**
             * Gets the "darahRutinTrom" element
             */
            public java.lang.String getDarahRutinTrom()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINTROM$22, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "darahRutinTrom" element
             */
            public org.apache.xmlbeans.XmlString xgetDarahRutinTrom()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINTROM$22, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "darahRutinTrom" element
             */
            public void setDarahRutinTrom(java.lang.String darahRutinTrom)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DARAHRUTINTROM$22, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DARAHRUTINTROM$22);
                    }
                    target.setStringValue(darahRutinTrom);
                }
            }
            
            /**
             * Sets (as xml) the "darahRutinTrom" element
             */
            public void xsetDarahRutinTrom(org.apache.xmlbeans.XmlString darahRutinTrom)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DARAHRUTINTROM$22, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DARAHRUTINTROM$22);
                    }
                    target.set(darahRutinTrom);
                }
            }
            
            /**
             * Gets the "lemakDarahHDL" element
             */
            public java.lang.String getLemakDarahHDL()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHHDL$24, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "lemakDarahHDL" element
             */
            public org.apache.xmlbeans.XmlString xgetLemakDarahHDL()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHHDL$24, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "lemakDarahHDL" element
             */
            public void setLemakDarahHDL(java.lang.String lemakDarahHDL)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHHDL$24, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LEMAKDARAHHDL$24);
                    }
                    target.setStringValue(lemakDarahHDL);
                }
            }
            
            /**
             * Sets (as xml) the "lemakDarahHDL" element
             */
            public void xsetLemakDarahHDL(org.apache.xmlbeans.XmlString lemakDarahHDL)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHHDL$24, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LEMAKDARAHHDL$24);
                    }
                    target.set(lemakDarahHDL);
                }
            }
            
            /**
             * Gets the "lemakDarahLDL" element
             */
            public java.lang.String getLemakDarahLDL()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHLDL$26, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "lemakDarahLDL" element
             */
            public org.apache.xmlbeans.XmlString xgetLemakDarahLDL()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHLDL$26, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "lemakDarahLDL" element
             */
            public void setLemakDarahLDL(java.lang.String lemakDarahLDL)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHLDL$26, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LEMAKDARAHLDL$26);
                    }
                    target.setStringValue(lemakDarahLDL);
                }
            }
            
            /**
             * Sets (as xml) the "lemakDarahLDL" element
             */
            public void xsetLemakDarahLDL(org.apache.xmlbeans.XmlString lemakDarahLDL)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHLDL$26, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LEMAKDARAHLDL$26);
                    }
                    target.set(lemakDarahLDL);
                }
            }
            
            /**
             * Gets the "lemakDarahChol" element
             */
            public java.lang.String getLemakDarahChol()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHCHOL$28, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "lemakDarahChol" element
             */
            public org.apache.xmlbeans.XmlString xgetLemakDarahChol()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHCHOL$28, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "lemakDarahChol" element
             */
            public void setLemakDarahChol(java.lang.String lemakDarahChol)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHCHOL$28, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LEMAKDARAHCHOL$28);
                    }
                    target.setStringValue(lemakDarahChol);
                }
            }
            
            /**
             * Sets (as xml) the "lemakDarahChol" element
             */
            public void xsetLemakDarahChol(org.apache.xmlbeans.XmlString lemakDarahChol)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHCHOL$28, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LEMAKDARAHCHOL$28);
                    }
                    target.set(lemakDarahChol);
                }
            }
            
            /**
             * Gets the "lemakDarahTrigli" element
             */
            public java.lang.String getLemakDarahTrigli()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHTRIGLI$30, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "lemakDarahTrigli" element
             */
            public org.apache.xmlbeans.XmlString xgetLemakDarahTrigli()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHTRIGLI$30, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "lemakDarahTrigli" element
             */
            public void setLemakDarahTrigli(java.lang.String lemakDarahTrigli)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LEMAKDARAHTRIGLI$30, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LEMAKDARAHTRIGLI$30);
                    }
                    target.setStringValue(lemakDarahTrigli);
                }
            }
            
            /**
             * Sets (as xml) the "lemakDarahTrigli" element
             */
            public void xsetLemakDarahTrigli(org.apache.xmlbeans.XmlString lemakDarahTrigli)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LEMAKDARAHTRIGLI$30, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LEMAKDARAHTRIGLI$30);
                    }
                    target.set(lemakDarahTrigli);
                }
            }
            
            /**
             * Gets the "gulaDarahSewaktu" element
             */
            public java.lang.String getGulaDarahSewaktu()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GULADARAHSEWAKTU$32, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "gulaDarahSewaktu" element
             */
            public org.apache.xmlbeans.XmlString xgetGulaDarahSewaktu()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(GULADARAHSEWAKTU$32, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "gulaDarahSewaktu" element
             */
            public void setGulaDarahSewaktu(java.lang.String gulaDarahSewaktu)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GULADARAHSEWAKTU$32, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GULADARAHSEWAKTU$32);
                    }
                    target.setStringValue(gulaDarahSewaktu);
                }
            }
            
            /**
             * Sets (as xml) the "gulaDarahSewaktu" element
             */
            public void xsetGulaDarahSewaktu(org.apache.xmlbeans.XmlString gulaDarahSewaktu)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(GULADARAHSEWAKTU$32, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(GULADARAHSEWAKTU$32);
                    }
                    target.set(gulaDarahSewaktu);
                }
            }
            
            /**
             * Gets the "gulaDarahPuasa" element
             */
            public java.lang.String getGulaDarahPuasa()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GULADARAHPUASA$34, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "gulaDarahPuasa" element
             */
            public org.apache.xmlbeans.XmlString xgetGulaDarahPuasa()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(GULADARAHPUASA$34, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "gulaDarahPuasa" element
             */
            public void setGulaDarahPuasa(java.lang.String gulaDarahPuasa)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GULADARAHPUASA$34, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GULADARAHPUASA$34);
                    }
                    target.setStringValue(gulaDarahPuasa);
                }
            }
            
            /**
             * Sets (as xml) the "gulaDarahPuasa" element
             */
            public void xsetGulaDarahPuasa(org.apache.xmlbeans.XmlString gulaDarahPuasa)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(GULADARAHPUASA$34, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(GULADARAHPUASA$34);
                    }
                    target.set(gulaDarahPuasa);
                }
            }
            
            /**
             * Gets the "gulaDarahPostPrandial" element
             */
            public java.lang.String getGulaDarahPostPrandial()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GULADARAHPOSTPRANDIAL$36, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "gulaDarahPostPrandial" element
             */
            public org.apache.xmlbeans.XmlString xgetGulaDarahPostPrandial()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(GULADARAHPOSTPRANDIAL$36, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "gulaDarahPostPrandial" element
             */
            public void setGulaDarahPostPrandial(java.lang.String gulaDarahPostPrandial)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GULADARAHPOSTPRANDIAL$36, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GULADARAHPOSTPRANDIAL$36);
                    }
                    target.setStringValue(gulaDarahPostPrandial);
                }
            }
            
            /**
             * Sets (as xml) the "gulaDarahPostPrandial" element
             */
            public void xsetGulaDarahPostPrandial(org.apache.xmlbeans.XmlString gulaDarahPostPrandial)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(GULADARAHPOSTPRANDIAL$36, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(GULADARAHPOSTPRANDIAL$36);
                    }
                    target.set(gulaDarahPostPrandial);
                }
            }
            
            /**
             * Gets the "fungsiHatiSGOT" element
             */
            public java.lang.String getFungsiHatiSGOT()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATISGOT$38, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiHatiSGOT" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiHatiSGOT()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATISGOT$38, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiHatiSGOT" element
             */
            public void setFungsiHatiSGOT(java.lang.String fungsiHatiSGOT)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATISGOT$38, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIHATISGOT$38);
                    }
                    target.setStringValue(fungsiHatiSGOT);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiHatiSGOT" element
             */
            public void xsetFungsiHatiSGOT(org.apache.xmlbeans.XmlString fungsiHatiSGOT)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATISGOT$38, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIHATISGOT$38);
                    }
                    target.set(fungsiHatiSGOT);
                }
            }
            
            /**
             * Gets the "fungsiHatiSGPT" element
             */
            public java.lang.String getFungsiHatiSGPT()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATISGPT$40, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiHatiSGPT" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiHatiSGPT()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATISGPT$40, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiHatiSGPT" element
             */
            public void setFungsiHatiSGPT(java.lang.String fungsiHatiSGPT)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATISGPT$40, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIHATISGPT$40);
                    }
                    target.setStringValue(fungsiHatiSGPT);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiHatiSGPT" element
             */
            public void xsetFungsiHatiSGPT(org.apache.xmlbeans.XmlString fungsiHatiSGPT)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATISGPT$40, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIHATISGPT$40);
                    }
                    target.set(fungsiHatiSGPT);
                }
            }
            
            /**
             * Gets the "fungsiHatiGamma" element
             */
            public java.lang.String getFungsiHatiGamma()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATIGAMMA$42, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiHatiGamma" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiHatiGamma()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATIGAMMA$42, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiHatiGamma" element
             */
            public void setFungsiHatiGamma(java.lang.String fungsiHatiGamma)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATIGAMMA$42, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIHATIGAMMA$42);
                    }
                    target.setStringValue(fungsiHatiGamma);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiHatiGamma" element
             */
            public void xsetFungsiHatiGamma(org.apache.xmlbeans.XmlString fungsiHatiGamma)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATIGAMMA$42, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIHATIGAMMA$42);
                    }
                    target.set(fungsiHatiGamma);
                }
            }
            
            /**
             * Gets the "fungsiHatiProtKua" element
             */
            public java.lang.String getFungsiHatiProtKua()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATIPROTKUA$44, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiHatiProtKua" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiHatiProtKua()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATIPROTKUA$44, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiHatiProtKua" element
             */
            public void setFungsiHatiProtKua(java.lang.String fungsiHatiProtKua)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATIPROTKUA$44, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIHATIPROTKUA$44);
                    }
                    target.setStringValue(fungsiHatiProtKua);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiHatiProtKua" element
             */
            public void xsetFungsiHatiProtKua(org.apache.xmlbeans.XmlString fungsiHatiProtKua)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATIPROTKUA$44, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIHATIPROTKUA$44);
                    }
                    target.set(fungsiHatiProtKua);
                }
            }
            
            /**
             * Gets the "fungsiHatiAlbumin" element
             */
            public java.lang.String getFungsiHatiAlbumin()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATIALBUMIN$46, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiHatiAlbumin" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiHatiAlbumin()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATIALBUMIN$46, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiHatiAlbumin" element
             */
            public void setFungsiHatiAlbumin(java.lang.String fungsiHatiAlbumin)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIHATIALBUMIN$46, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIHATIALBUMIN$46);
                    }
                    target.setStringValue(fungsiHatiAlbumin);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiHatiAlbumin" element
             */
            public void xsetFungsiHatiAlbumin(org.apache.xmlbeans.XmlString fungsiHatiAlbumin)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIHATIALBUMIN$46, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIHATIALBUMIN$46);
                    }
                    target.set(fungsiHatiAlbumin);
                }
            }
            
            /**
             * Gets the "fungsiGinjalCrea" element
             */
            public java.lang.String getFungsiGinjalCrea()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIGINJALCREA$48, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiGinjalCrea" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiGinjalCrea()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIGINJALCREA$48, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiGinjalCrea" element
             */
            public void setFungsiGinjalCrea(java.lang.String fungsiGinjalCrea)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIGINJALCREA$48, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIGINJALCREA$48);
                    }
                    target.setStringValue(fungsiGinjalCrea);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiGinjalCrea" element
             */
            public void xsetFungsiGinjalCrea(org.apache.xmlbeans.XmlString fungsiGinjalCrea)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIGINJALCREA$48, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIGINJALCREA$48);
                    }
                    target.set(fungsiGinjalCrea);
                }
            }
            
            /**
             * Gets the "fungsiGinjalUreum" element
             */
            public java.lang.String getFungsiGinjalUreum()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIGINJALUREUM$50, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiGinjalUreum" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiGinjalUreum()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIGINJALUREUM$50, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiGinjalUreum" element
             */
            public void setFungsiGinjalUreum(java.lang.String fungsiGinjalUreum)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIGINJALUREUM$50, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIGINJALUREUM$50);
                    }
                    target.setStringValue(fungsiGinjalUreum);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiGinjalUreum" element
             */
            public void xsetFungsiGinjalUreum(org.apache.xmlbeans.XmlString fungsiGinjalUreum)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIGINJALUREUM$50, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIGINJALUREUM$50);
                    }
                    target.set(fungsiGinjalUreum);
                }
            }
            
            /**
             * Gets the "fungsiGinjalAsam" element
             */
            public java.lang.String getFungsiGinjalAsam()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIGINJALASAM$52, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiGinjalAsam" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiGinjalAsam()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIGINJALASAM$52, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiGinjalAsam" element
             */
            public void setFungsiGinjalAsam(java.lang.String fungsiGinjalAsam)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIGINJALASAM$52, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIGINJALASAM$52);
                    }
                    target.setStringValue(fungsiGinjalAsam);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiGinjalAsam" element
             */
            public void xsetFungsiGinjalAsam(org.apache.xmlbeans.XmlString fungsiGinjalAsam)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIGINJALASAM$52, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIGINJALASAM$52);
                    }
                    target.set(fungsiGinjalAsam);
                }
            }
            
            /**
             * Gets the "fungsiJantungABI" element
             */
            public java.lang.String getFungsiJantungABI()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIJANTUNGABI$54, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiJantungABI" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiJantungABI()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIJANTUNGABI$54, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiJantungABI" element
             */
            public void setFungsiJantungABI(java.lang.String fungsiJantungABI)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIJANTUNGABI$54, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIJANTUNGABI$54);
                    }
                    target.setStringValue(fungsiJantungABI);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiJantungABI" element
             */
            public void xsetFungsiJantungABI(org.apache.xmlbeans.XmlString fungsiJantungABI)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIJANTUNGABI$54, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIJANTUNGABI$54);
                    }
                    target.set(fungsiJantungABI);
                }
            }
            
            /**
             * Gets the "fungsiJantungEKG" element
             */
            public java.lang.String getFungsiJantungEKG()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIJANTUNGEKG$56, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiJantungEKG" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiJantungEKG()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIJANTUNGEKG$56, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiJantungEKG" element
             */
            public void setFungsiJantungEKG(java.lang.String fungsiJantungEKG)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIJANTUNGEKG$56, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIJANTUNGEKG$56);
                    }
                    target.setStringValue(fungsiJantungEKG);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiJantungEKG" element
             */
            public void xsetFungsiJantungEKG(org.apache.xmlbeans.XmlString fungsiJantungEKG)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIJANTUNGEKG$56, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIJANTUNGEKG$56);
                    }
                    target.set(fungsiJantungEKG);
                }
            }
            
            /**
             * Gets the "fungsiJantungEcho" element
             */
            public java.lang.String getFungsiJantungEcho()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIJANTUNGECHO$58, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fungsiJantungEcho" element
             */
            public org.apache.xmlbeans.XmlString xgetFungsiJantungEcho()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIJANTUNGECHO$58, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fungsiJantungEcho" element
             */
            public void setFungsiJantungEcho(java.lang.String fungsiJantungEcho)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNGSIJANTUNGECHO$58, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNGSIJANTUNGECHO$58);
                    }
                    target.setStringValue(fungsiJantungEcho);
                }
            }
            
            /**
             * Sets (as xml) the "fungsiJantungEcho" element
             */
            public void xsetFungsiJantungEcho(org.apache.xmlbeans.XmlString fungsiJantungEcho)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNGSIJANTUNGECHO$58, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNGSIJANTUNGECHO$58);
                    }
                    target.set(fungsiJantungEcho);
                }
            }
            
            /**
             * Gets the "urinRutin" element
             */
            public java.lang.String getUrinRutin()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(URINRUTIN$60, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "urinRutin" element
             */
            public org.apache.xmlbeans.XmlString xgetUrinRutin()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(URINRUTIN$60, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "urinRutin" element
             */
            public void setUrinRutin(java.lang.String urinRutin)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(URINRUTIN$60, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(URINRUTIN$60);
                    }
                    target.setStringValue(urinRutin);
                }
            }
            
            /**
             * Sets (as xml) the "urinRutin" element
             */
            public void xsetUrinRutin(org.apache.xmlbeans.XmlString urinRutin)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(URINRUTIN$60, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(URINRUTIN$60);
                    }
                    target.set(urinRutin);
                }
            }
            
            /**
             * Gets the "fundusKopi" element
             */
            public java.lang.String getFundusKopi()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNDUSKOPI$62, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "fundusKopi" element
             */
            public org.apache.xmlbeans.XmlString xgetFundusKopi()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNDUSKOPI$62, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "fundusKopi" element
             */
            public void setFundusKopi(java.lang.String fundusKopi)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FUNDUSKOPI$62, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FUNDUSKOPI$62);
                    }
                    target.setStringValue(fundusKopi);
                }
            }
            
            /**
             * Sets (as xml) the "fundusKopi" element
             */
            public void xsetFundusKopi(org.apache.xmlbeans.XmlString fundusKopi)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FUNDUSKOPI$62, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FUNDUSKOPI$62);
                    }
                    target.set(fundusKopi);
                }
            }
            
            /**
             * Gets the "pemeriksaanLain" element
             */
            public java.lang.String getPemeriksaanLain()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PEMERIKSAANLAIN$64, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "pemeriksaanLain" element
             */
            public org.apache.xmlbeans.XmlString xgetPemeriksaanLain()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PEMERIKSAANLAIN$64, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "pemeriksaanLain" element
             */
            public void setPemeriksaanLain(java.lang.String pemeriksaanLain)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PEMERIKSAANLAIN$64, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PEMERIKSAANLAIN$64);
                    }
                    target.setStringValue(pemeriksaanLain);
                }
            }
            
            /**
             * Sets (as xml) the "pemeriksaanLain" element
             */
            public void xsetPemeriksaanLain(org.apache.xmlbeans.XmlString pemeriksaanLain)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PEMERIKSAANLAIN$64, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PEMERIKSAANLAIN$64);
                    }
                    target.set(pemeriksaanLain);
                }
            }
            
            /**
             * Gets the "keterangan" element
             */
            public java.lang.String getKeterangan()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KETERANGAN$66, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "keterangan" element
             */
            public org.apache.xmlbeans.XmlString xgetKeterangan()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KETERANGAN$66, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "keterangan" element
             */
            public void setKeterangan(java.lang.String keterangan)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KETERANGAN$66, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KETERANGAN$66);
                    }
                    target.setStringValue(keterangan);
                }
            }
            
            /**
             * Sets (as xml) the "keterangan" element
             */
            public void xsetKeterangan(org.apache.xmlbeans.XmlString keterangan)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KETERANGAN$66, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KETERANGAN$66);
                    }
                    target.set(keterangan);
                }
            }
        }
        /**
         * An XML metadata(@http://www.bpjskes.com/pcare/GetDatMCU).
         *
         * This is a complex type.
         */
        public static class MetadataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.bpjskes.pcare.getDatMCU.Output.Response.Metadata
        {
            private static final long serialVersionUID = 1L;
            
            public MetadataImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName MESSAGE$0 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "message");
            private static final javax.xml.namespace.QName CODE$2 = 
                new javax.xml.namespace.QName("http://www.bpjskes.com/pcare/GetDatMCU", "code");
            
            
            /**
             * Gets the "message" element
             */
            public java.lang.String getMessage()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGE$0, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "message" element
             */
            public org.apache.xmlbeans.XmlString xgetMessage()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MESSAGE$0, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "message" element
             */
            public void setMessage(java.lang.String message)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGE$0, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MESSAGE$0);
                    }
                    target.setStringValue(message);
                }
            }
            
            /**
             * Sets (as xml) the "message" element
             */
            public void xsetMessage(org.apache.xmlbeans.XmlString message)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MESSAGE$0, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MESSAGE$0);
                    }
                    target.set(message);
                }
            }
            
            /**
             * Gets the "code" element
             */
            public java.lang.String getCode()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$2, 0);
                    if (target == null)
                    {
                      return null;
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) the "code" element
             */
            public org.apache.xmlbeans.XmlString xgetCode()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODE$2, 0);
                    return target;
                }
            }
            
            /**
             * Sets the "code" element
             */
            public void setCode(java.lang.String code)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$2, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODE$2);
                    }
                    target.setStringValue(code);
                }
            }
            
            /**
             * Sets (as xml) the "code" element
             */
            public void xsetCode(org.apache.xmlbeans.XmlString code)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlString target = null;
                    target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODE$2, 0);
                    if (target == null)
                    {
                      target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CODE$2);
                    }
                    target.set(code);
                }
            }
        }
    }
}
