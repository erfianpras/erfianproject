package com.datmcu.pojo;

public class GetDatMCU_Request {

	private String request;
	private String noKunjungan;
	
	public String getRequest(){
		return request;
	}
	public void setRequest(String Request){
		this.request = Request;
	}
	public String getNoKunjungan(){
		return noKunjungan;
	}
	public void setNoKunjungan(String NoKunjungan){
		this.noKunjungan=NoKunjungan;
	}
}
