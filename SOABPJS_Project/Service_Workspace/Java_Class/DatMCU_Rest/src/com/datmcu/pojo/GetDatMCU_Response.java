package com.datmcu.pojo;

import java.util.ArrayList;
import java.util.List;

public class GetDatMCU_Response {

	private String response;
	private String count;
	private List<Lists> list;	
//	private Metadata metadata;
//	
	public String getResponse(){
		return response;
	}
	public void setResponse(String Response){
		this.response = Response;
	}
	
	public String getCount(){
		return count;
	}
	public void setCount(String Count){
		this.count=Count;
	}

	public List<Lists> getList(){
		return list;
	}
	public void setList(List<Lists> Lists){
		this.list = Lists;
	}
//	public List<String> getLists(){
//		public String getKdMCU(){
//			return kdMCU;
//		}
//		public void setKdMCU(String KdMCU){
//			this.kdMCU = KdMCU;
//		}
//		return list;
//	}
//	public void setLists(List<String> Lists){
//		this.list = Lists;
//	}
//	
//	public Metadata getMetadata(){
//		return metadata;
//	}
//	public void setMetadata(Metadata Metadat){
//		this.metadata = Metadat;
//	}	
	
	public static class Lists { 
//		private List<String> list = new ArrayList<String>();
		private String kdMCU, noKunjungan, kdProvider, tglPelayanan, tekananDarahSistole, radiologiFoto
		, darahRutinHemo, darahRutinLeu, darahRutinErit, darahRutinLaju, darahRutinHema, darahRutinTrom
		, lemakDarahHDL, lemakDarahLDL, lemakDarahChol, lemakDarahTrigli, gulaDarahSewaktu, gulaDarahPuasa
		, gulaDarahPostPrandial, fungsiHatiSGOT, fungsiHatiSGPT, fungsiHatiGamma, fungsiHatiProtKua
		, fungsiHatiAlbumin, fungsiGinjalCrea, fungsiGinjalUreum, fungsiGinjalAsam, fungsiJantungABI
		, fungsiJantungEKG, fungsiJantungEcho, urinRutin, fundusKopi, pemeriksaanLain, keterangan;
		
		public String getKdMCU(){
			return kdMCU;
		}
		public void setKdMCU(String KdMCU){
			this.kdMCU = KdMCU;
		}
		
		public String getNoKunjungan(){
			return noKunjungan;
		}
		public void setNoKunjungan(String NoKunjungan){
			this.noKunjungan = NoKunjungan;
		}
		
		public String getKdProvider(){
			return kdProvider;
		}
		public void setKdProvider(String KdProvider){
			this.kdProvider = KdProvider;
		}
		
		public String getTglPelayanan(){
			return tglPelayanan;
		}
		public void setTglPelayanan(String TglPelayanan){
			this.tglPelayanan = TglPelayanan;
		}
		
		public String getTekananDarahSistole(){
			return tekananDarahSistole;
		}
		public void setTekananDarahSistole(String TekananDarahSistole){
			this.tekananDarahSistole = TekananDarahSistole;
		}
		
		public String getRadiologiFoto(){
			return radiologiFoto;
		}
		public void setRadiologiFoto(String RadiologiFoto){
			this.radiologiFoto = RadiologiFoto;
		}
		
		public String getDarahRutinHemo(){
			return darahRutinHemo;
		}
		public void setDarahRutinHemo(String DarahRutinHemo){
			this.darahRutinHemo = DarahRutinHemo;
		}
		
		public String getDarahRutinLeu(){
			return darahRutinLeu;
		}
		public void setDarahRutinLeu(String DarahRutinLeu){
			this.darahRutinLeu = DarahRutinLeu;
		}
		
		public String getDarahRutinErit(){
			return darahRutinErit;
		}
		public void getDarahRutinErit(String DarahRutinErit){
			this.darahRutinErit = DarahRutinErit;
		}
		
		public String getDarahRutinLaju(){
			return darahRutinLaju;
		}
		public void setDarahRutinLaju(String DarahRutinLaju){
			this.darahRutinLaju = DarahRutinLaju;
		}
		
		public String getDarahRutinHema(){
			return darahRutinHema;
		}
		public void setDarahRutinHema(String DarahRutinHema){
			this.darahRutinHema = DarahRutinHema;
		}
		
		public String getDarahRutinTrom(){
			return darahRutinTrom;
		}
		public void setDarahRutinTrom(String DarahRutinTrom){
			this.darahRutinTrom = DarahRutinTrom;
		}
		
		public String getLemakDarahHDL(){
			return lemakDarahHDL;
		}
		public void setLemakDarahHDL(String LemakDarahHDL){
			this.lemakDarahHDL = LemakDarahHDL;
		}
		
		public String getLemakDarahLDL(){
			return lemakDarahLDL;
		}
		public void setLemakDarahLDL(String LemakDarahLDL){
			this.lemakDarahLDL = LemakDarahLDL;
		}
		
		public String getLemakDarahChol(){
			return lemakDarahChol;
		}
		public void setLemakDarahChol(String LemakDarahChol){
			this.lemakDarahChol = LemakDarahChol;
		}
		
		public String getLemakDarahTrigli(){
			return lemakDarahTrigli;
		}
		public void setLemakDarahTrigli(String LemakDarahTrigli){
			this.lemakDarahTrigli = LemakDarahTrigli;
		}
		
		public String getGulaDarahSewaktu(){
			return gulaDarahSewaktu;
		}
		public void setGulaDarahSewaktu(String GulaDarahSewaktu){
			this.gulaDarahSewaktu = GulaDarahSewaktu;
		}
		
		public String getGulaDarahPuasa(){
			return gulaDarahPuasa;
		}
		public void setGulaDarahPuasa(String GulaDarahPuasa){
			this.gulaDarahPuasa = GulaDarahPuasa;
		}
		
		public String getGulaDarahPostPrandial(){
			return gulaDarahPostPrandial;
		}
		public void setGulaDarahPostPrandial(String GulaDarahPostPrandial){
			this.gulaDarahPostPrandial = GulaDarahPostPrandial;
		}
		
		public String getFungsiHatiSGOT(){
			return fungsiHatiSGOT;
		}
		public void setFungsiHatiSGOT(String FungsiHatiSGOT){
			this.fungsiHatiSGOT = FungsiHatiSGOT;
		}
		
		public String getFungsiHatiSGPT(){
			return fungsiHatiSGPT;
		}
		public void setFungsiHatiSGPT(String FungsiHatiSGPT){
			this.fungsiHatiSGPT = FungsiHatiSGPT;
		}
		
		public String getFungsiHatiGamma(){
			return fungsiHatiGamma;
		}
		public void setFungsiHatiGamma(String FungsiHatiGamma){
			this.fungsiHatiGamma = FungsiHatiGamma;
		}
		
		public String getFungsiHatiProtKua(){
			return fungsiHatiProtKua;
		}
		public void setFungsiHatiProtKua(String FungsiHatiProtKua){
			this.fungsiHatiProtKua = FungsiHatiProtKua;
		}
		
		public String getFungsiHatiAlbumin(){
			return fungsiHatiAlbumin;
		}
		public void setFungsiHatiAlbumin(String FungsiHatiAlbumin){
			this.fungsiHatiAlbumin = FungsiHatiAlbumin;
		}
		
		public String getFungsiGinjalCrea(){
			return fungsiGinjalCrea;
		}
		public void setFungsiGinjalCrea(String FungsiGinjalCrea){
			this.fungsiGinjalCrea = FungsiGinjalCrea;
		}
		
		public String getFungsiGinjalUreum(){
			return fungsiGinjalUreum;
		}
		public void setFungsiGinjalUreum(String FungsiGinjalUreum){
			this.fungsiGinjalUreum = FungsiGinjalUreum;
		}
		
		public String getFungsiGinjalAsam(){
			return fungsiGinjalAsam;
		}
		public void setFungsiGinjalAsam(String FungsiGinjalAsam){
			this.fungsiGinjalAsam = FungsiGinjalAsam;
		}
		
		public String getFungsiJantungABI(){
			return fungsiJantungABI;
		}
		public void setFungsiJantungABI(String FungsiJantungABI){
			this.fungsiJantungABI = FungsiJantungABI;
		}
		
		public String getFungsiJantungEKG(){
			return fungsiJantungEKG;
		}
		public void setFungsiJantungEcho(String FungsiJantungEcho){
			this.fungsiJantungEcho = FungsiJantungEcho;
		}
		
		public String getUrinRutin(){
			return urinRutin;
		}
		public void setUrinRutin(String UrinRutin){
			this.urinRutin = UrinRutin;
		}
		
		public String getFundusKopi(){
			return fundusKopi;
		}
		public void setFundusKopi(String FundusKopi){
			this.fundusKopi = FundusKopi;
		}
		
		public String getPemeriksaanLain(){
			return pemeriksaanLain;
		}
		public void setPemeriksaanLain(String PemeriksaanLain){
			this.pemeriksaanLain = PemeriksaanLain;
		}
		
		public String getKeterangan(){
			return keterangan;
		}
		public void setKeterangan(String Keterangan){
			this.keterangan = Keterangan;
		}
	}
	
//	public class Metadata{
		
		private String metadata;
		private String message;
		private String code;
		
		public String getMetadata(){
			return metadata;
		}
		public void setMetadata(String Metadata){
			this.metadata = Metadata;
		}
		
		public String getMessage(){
			return message;
		}
		public void setMessage(String Message){
			this.message = Message;
		}
		
		public String getCode(){
			return code;
		}
		public void setCode(String Code){
			this.code = Code;
		}
	}
	
//}
