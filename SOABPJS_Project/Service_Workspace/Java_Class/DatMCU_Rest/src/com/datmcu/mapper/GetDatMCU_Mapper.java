package com.datmcu.mapper;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.datmcu.pojo.GetDatMCU_Request;
import com.datmcu.pojo.GetDatMCU_Response;

public class GetDatMCU_Mapper {

	private static ObjectMapper mapper;
	private static ObjectWriter datMCURequestWriter, datMCUResponseWriter;
	private static ObjectReader datMCURequestReader, datMCUResponseReader;
	
	static{
		mapper = new ObjectMapper();
		mapper.setSerializationConfig(mapper.getSerializationConfig().
				withSerializationInclusion(Inclusion.ALWAYS));
		datMCURequestReader = mapper.reader(GetDatMCU_Request.class);
		datMCURequestWriter = mapper.writerWithType(GetDatMCU_Request.class);
		datMCUResponseReader = mapper.reader(GetDatMCU_Response.class);
		datMCUResponseWriter = mapper.writerWithType(GetDatMCU_Response.class);
	}
	
	public static ObjectReader getDatMCURequestReader(){
		return datMCURequestReader;
	}
	public static ObjectWriter getDatMCURequestWriter(){
		return datMCURequestWriter;
	}
	public static ObjectReader getDatMCUResponseReader(){
		return datMCUResponseReader;
	}
	public static ObjectWriter getDatMCUResponseWriter(){
		return datMCUResponseWriter;
	}
}
