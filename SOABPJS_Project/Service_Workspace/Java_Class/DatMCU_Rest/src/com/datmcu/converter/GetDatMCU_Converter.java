package com.datmcu.converter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.SerializationConfig;
import org.json.JSONObject;


import com.bpjskes.pcare.getDatMCU.Input;
import com.bpjskes.pcare.getDatMCU.Output;
import com.bpjskes.pcare.getDatMCU.ProcessRequestDocument;
import com.bpjskes.pcare.getDatMCU.ProcessResponseDocument;

import com.datmcu.pojo.GetDatMCU_Request;
import com.datmcu.pojo.GetDatMCU_Response;
import com.datmcu.mapper.GetDatMCU_Mapper;



public class GetDatMCU_Converter {

	public static XmlObject getDatMCUGetJsonToXml(String json){
		ObjectReader reader = GetDatMCU_Mapper.getDatMCURequestReader();
		ProcessRequestDocument requestDoc = ProcessRequestDocument.Factory.newInstance();
		GetDatMCU_Request datMCUReq;
		try{
			datMCUReq = reader.readValue(json);
			System.out.println(json);
			Input jsonInput = Input.Factory.newInstance();
			jsonInput.addNewRequest().setNoKunjungan(datMCUReq.getNoKunjungan());
			requestDoc.setProcessRequest(jsonInput);
			System.out.println(requestDoc);
		}
		catch(JsonProcessingException jse){
			jse.printStackTrace();
		}
		catch (Exception e) {
			 e.printStackTrace();
		}
		return requestDoc;		
	}
	
	public static String getDatMCUXmlToJson(XmlObject xml){
		ObjectWriter writer = GetDatMCU_Mapper.getDatMCUResponseWriter();
		GetDatMCU_Response datMCUResp = new GetDatMCU_Response();
//		GetDatMCU_Response.Lists datMCURespList = new GetDatMCU_Response.Lists();
		String responJson = null;
		
		try{
			XmlObject doc = XmlObject.Factory.parse(xml.newXMLStreamReader());
			
			if (doc instanceof ProcessResponseDocument){
				ProcessResponseDocument ResponseDoc = (ProcessResponseDocument) doc;
				Output response = ResponseDoc.getProcessResponse();
				datMCUResp.setResponse("{");				
				datMCUResp.setCount(response.getResponse().getCount());
				List<GetDatMCU_Response.Lists> datMCURespList = new ArrayList<GetDatMCU_Response.Lists>();
				datMCURespList.setKdMCU(response.getResponse().getList().getKdMCU());
				datMCURespList.setNoKunjungan(response.getResponse().getList().getNoKunjungan());
				datMCURespList.setKdProvider(response.getResponse().getList().getKdProvider());
				datMCUResp.setList(new ArrayList<GetDatMCU_Response.Lists>());
				
//				datMCUResp.setKdMCU(response.getResponse().getList().getKdMCU());
//				datMCURespList.setNoKunjungan(response.getResponse().getList().getNoKunjungan());
//				datMCUResp.setKdProvider(response.getResponse().getList().getKdProvider());
				
				datMCUResp.setMetadata(null);
				datMCUResp.setMessage(response.getResponse().getMetadata().getMessage());			
				responJson = writer.writeValueAsString(datMCUResp);
			}
			else{
			System.out.println("GetDatMCU_XMLToJson(): PARSE FAILED!!!");
			}
		}
		catch(XmlException xe){
			xe.printStackTrace();
		}
		catch(JsonMappingException jsme){
			jsme.printStackTrace();
		}
		catch(IOException ie){
			ie.printStackTrace();
		}
		return responJson;
	}
}
